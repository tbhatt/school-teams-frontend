import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import {
  BrowserRouter as Router,
  Switch,
  Route, Redirect
} from "react-router-dom";

import FrontPage from './components/login_flow/front_page/FrontPage.js';
import LoginPage from './components/login_flow/login_page/LoginPage.js';
import Auth from './components/login_flow/login_page/Auth.js';

import RegistrationDonePage from './components/login_flow/confirmation_pages/RegistrationDonePage.js';
import ForgotPassword from './components/login_flow/reset_pages/ForgotPassword.js';
import ResetPassword from './components/login_flow/reset_pages/ResetPassword.js';
import HomePage from './components/home_page/HomePage.js';
import GenericError from './components/error_pages/GenericError.js';

import ProfilePage from './components/profile_page/Profile_Page.js';

//Admin pages
import SchoolPage from './components/admin/manage_schools_page/SchoolPage.js';
import AdminInboxPage from './components/admin/admin_inbox_page/AdminInboxPage.js';

//Admin and coach pages
import LocationPage from './components/admin_coach/manage_locations_page/LocationPage.js';
import UpdateSportPage from './components/admin_coach/update_sport_page/UpdateSportPage.js';
import UpdateTeamPage from './components/admin_coach/update_teams_page/UpdateTeamPage.js';
import CancelPage from './components/admin_coach/cancel_matches_page/CancelPage.js';
import NotifyPage from './components/admin_coach/notify_parents_page/NotifyPage.js';
import CreateUsersPage from './components/admin_coach/create_players_parents_page/CreateUsersPage.js';

//Parent and player pages
import MessagePage from './components/parent_player/message_page/MessagePage.js';
import MatchesPage from './components/parent_player/matches_page/MatchesPage.js';

//Coach pages
import CreateMatchPage from './components/coach/create_matches/CreateMatchPage.js';
import StatsPage from './components/coach/statistics_page/StatsPage.js';
import RecordMatchPage from './components/coach/record_results/RecordMatchPage.js';
import AssignTeamPage from './components/coach/assign_player_team/AssignTeamPage.js';
import SettingsPage from './components/home_page/SettingsPage.js';


function App() {
  document.title = "School Teams";
  const authGuard = (Component) => () => {
    return sessionStorage.getItem('token') ? (
      <Component/>
    ) : (
      <Redirect to="/login"/>
    );    
  };

  const reverseAuthGuard = (Component) => () => {
    return !sessionStorage.getItem('token') ? (
      <Component/>
    ) : (
      <Redirect to="/home"/>
    );    
  };

  



  return (
    <Router>
      <div>
        <Switch>
          <Route exact path="/"
            render = {reverseAuthGuard(FrontPage)}>
          </Route>
          <Route path="/login"
            render = {reverseAuthGuard(LoginPage)}>
          </Route>
          <Route path="/forgotpw"
            render = {reverseAuthGuard(ForgotPassword)}>
          </Route>
          <Route path="/resetpw/:token"
            render = {reverseAuthGuard(ResetPassword)}>
          </Route>
          <Route path="/auth">
            <Auth></Auth>
          </Route>
          <Route path="/complete"
            render = {reverseAuthGuard(RegistrationDonePage)}>
          </Route>
          <Route path="/home"
              render = {authGuard(HomePage)}>
            </Route>
            <Route path="/settings"
              render = {authGuard(SettingsPage)}>
            </Route>
            <Route path="/locations"
              render = {authGuard(LocationPage)}>
            </Route>
            <Route path="/schools"
              render = {authGuard(SchoolPage)}>
            </Route>
            <Route path="/stats"
              render = {authGuard(StatsPage)}>
            </Route>
            <Route path="/corrections"
              render = {authGuard(AdminInboxPage)}>
            </Route>
            <Route path="/profile/:username"
              render = {authGuard(ProfilePage)}>
            </Route>
            <Route path="/create/match"
              render = {authGuard(CreateMatchPage)}>
            </Route>
            <Route path="/record/match"
              render = {authGuard(RecordMatchPage)}>
            </Route>
            <Route path="/assign/team"
              render = {authGuard(AssignTeamPage)}>
            </Route>
            <Route path = "/sports"
              render = {authGuard(UpdateSportPage)}>
            </Route>
            <Route path = "/teams"
              render = {authGuard(UpdateTeamPage)}>
            </Route>
            <Route path = "/cancel"
              render = {authGuard(CancelPage)}>
            </Route>
            <Route path = "/notify"
              render = {authGuard(NotifyPage)}>
            </Route>
            <Route path = "/message"
              render = {authGuard(MessagePage)}>  
            </Route>
            <Route path = "/matches"
              render = {authGuard(MatchesPage)}>  
            </Route>
            <Route path = "/create/users"
              render = {authGuard(CreateUsersPage)}>
            </Route>
          <Route path="*">
            <GenericError></GenericError>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;

