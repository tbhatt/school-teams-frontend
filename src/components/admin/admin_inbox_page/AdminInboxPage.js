import React, { useEffect, useState } from 'react';
import { Button, Card, Container } from 'react-bootstrap';
import InboxComponent from '../../parent_player/message_page/InboxComponent';
import * as URL from '../../shared/Const.js';
import '../../admin_coach/manage_locations_page/Location.css';
import { useHistory } from 'react-router-dom';

function AdminInboxPage(){
    const [inbox, setInbox] = useState([]);
    const [personalInbox, setPersonalInbox] = useState([]);
    const [personal, setPersonal] = useState(false);

    const baseURL = URL.ADMIN_URL;
    const history = useHistory();

    useEffect(()=> {
        fetch(baseURL + "message/received/administrator", {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
        }).then(function(response){
            if (response.status === 401){
                alert("You don't have access to this page!");
                history.push('/home');
            } else {
                return response.json();
            }
        }).then(function(data){
            setInbox(data);
            return fetch(baseURL + "message/received/administrator/" + sessionStorage.getItem("user"), {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            })
        }).then(function(res){
            return res.json()
        }).then(function(data){
            setPersonalInbox(data);
            return;
        })
    }, [])

    function toggleMessages(){
        if (personal){
            setPersonal(false);
        } else {
            setPersonal(true);
        }
    }

    return (
        <div id = "locationBG"> 
        <Container>
        <h1 id = "white">Inbox</h1>
        </Container>
            <div id ="maginInbox">

                {!personal && <InboxComponent inbox = {inbox}></InboxComponent>}
                {personal && <InboxComponent inbox = {personalInbox}></InboxComponent>}
                <div id = "thign">
                    {!personal && <Button variant="dark" onClick={toggleMessages}>Filter to personal messages</Button>}
                    {personal && <Button variant="dark" onClick={toggleMessages}>Show all administrator messages</Button>}


                </div>



            </div>  

        </div>)
}

export default AdminInboxPage;