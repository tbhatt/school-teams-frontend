import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap';
import '../../admin_coach/manage_locations_page/Location.css';

const EditSchool = (props) => {
    //True = add, False = Edit
    //const add = true;
    
    const[name, setName] = useState("");
    const[school, setSchool] = useState(props.school);
    const[lock, setLock] = useState(true);
    const[hide, setHide] = useState(true); //Hide buttons

    useEffect(() => { 
        setSchool(props.school);
        setName(props.school.name);
        console.log("Operation: " + props.operation)

        if(props.operation) {
            console.log("Locks to false")
            setLock(false);
            setHide(false);
        }
        else{
            console.log("Locks to true")

            setLock(true);
            setHide(true);
        }
    
    }, [props]);

    const submit = () => {
        let result = {
            name: name
        }
        console.log("Operation: " + props.operation)
        console.log("Lock: " + lock)
        if(!lock) {
            console.log("Adding ID")
            result.id = props.school.id
        }
        props.setSchoolInfo(result);
    }

    const unlock = () => {
        setLock(!lock);
    }
    
    return (
        <form><fieldset disabled={lock}>
            <div class="form-row">
                <div class="col">
                    <input type="text" class="form-control" placeholder="Name"
                     onChange={e => setName(e.target.value)}
                     value={name}
                    />
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <Button onClick={submit} variant="outline-dark">
                        Submit
                    </Button>  
                </div>
                <div class="col">
                    {hide && <Button hide="true" onClick={submit} variant="outline-dark">
                        Delete
                    </Button>}   
                </div>
            </div>
      
            </fieldset>

            <div class="form-row">
                <div class="col">
                {hide && <Button onClick={unlock} variant="dark">
                    Edit
                </Button>}
                </div>
                <div class="col">
                <Button onClick={props.addSchoolClicked} variant="dark">
                        Add new school
                </Button> 
                </div>
            </div>
   
            </form>
    )
}
export default EditSchool
