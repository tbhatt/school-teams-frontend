import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Button, Card } from 'react-bootstrap';
import EditSchool from './EditSchool';
import  SchoolView  from './SchoolView';
import * as URL from '../../shared/Const.js';
import '../../admin_coach/manage_locations_page/Location.css';
import { useHistory } from 'react-router-dom';

export const SchoolPage = () => {
    const [schools, setSchools] = useState(); //All schools 
    const [school, setSchool] = useState(); //Selected school
    const [loading, setLoading] = useState(true); //Loading
    const [operation, setOperation] = useState(false); //True==Add, False==Edit
    const newSchool = {
        name: ""
    }
    const history = useHistory();

    useEffect(() => {
        getSchools();
    }, [])
    
    const getSchools = () => { //Gets all schools
        console.log("Getting schools")
        Axios.get(URL.ADMIN_URL + 'school', {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
        })
            .then( response => {
                if (response.status === 401){
                    alert("You don't have access to this page!");
                    history.push('/home');
                } 
                setSchools(response.data);
                setSchool(response.data[0]);
                setLoading(false);
            })
    }

    const setSchoolInfo = async (result) => {
        if(operation){ //Add new school
            delete result["id"]; //FIX FOR NOW. FIX EDIT SCHOOL. ID SHOULDNT COME
            console.log(result);
            await Axios.post(URL.ADMIN_URL + 'school', result, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            })
        }

        else{ //Edit school
            console.log("Updating school with ID: " + result.id)
            await Axios.put(URL.ADMIN_URL + `school/${result.id}`, result, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            })
        }
        await getSchools();   
    }

    const schoolClicked = (schoolInfo) => { 
        console.log("school clicked");
        console.log(schoolInfo);
        setSchool(schoolInfo);
        setOperation(false);
    }

    const addSchoolClicked = () => {
        console.log("Clicked add a new school")
        setSchool(newSchool);
        setOperation(true);
    }

    if(loading){
        return <div>Loading...</div>
    }

    return (
        <div id = "locationBG">
            <div class="container">
                <h1 id = "whiteFont">Schools</h1>
                <Card>
                    <h3>School details</h3>
                    <EditSchool addSchoolClicked={addSchoolClicked} school={school} operation={operation} setSchoolInfo={setSchoolInfo}></EditSchool>                
                
                </Card>

                <h1 id = "whiteFont">Click a school to manage it </h1>
                <Card id="locations-list">
 
                    {schools.map(school => <SchoolView key={school.id} school={school} schoolClick={schoolClicked}></SchoolView>)}
            
                </Card>
               
            </div>
        </div>
    )
}
export default SchoolPage
