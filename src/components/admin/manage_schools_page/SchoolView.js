import React from 'react'
import { ListGroup } from 'react-bootstrap';
import '../../admin_coach/manage_locations_page/Location.css';

export const SchoolView = (props) => {
    const school = props.school;

    const schoolSelected = () => {
        props.schoolClick(school);
    }

    return (
        <ListGroup.Item id = "half" onClick={schoolSelected}>
                Name: {school.name} 
        </ListGroup.Item>
    )
    
}
export default SchoolView
