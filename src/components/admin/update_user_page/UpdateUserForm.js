import React, { useState } from 'react';
import { Button, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import * as URL from '../../shared/Const.js';

import '../../admin_coach/create_players_parents_page/CreateUsers.css';


function UpdateUserForm(props){
    const baseURL = URL.ADMIN_URL;

    const user = props.user;
    const username = props.username;
    const fullUser = props.fullUser;
    let [newUsername, setNewUsername] = useState(fullUser.username);

    let [newFirstName, setNewFirstName] = useState(user.firstname);
    let [newLastName, setNewLastName] = useState(user.lastname);
    let [newEmail, setNewEmail] = useState(user.email);
    let [newGender, setNewGender] = useState(user.gender);

    function changeUsername(event){
        setNewUsername(event.target.value);
    }

    function submitUsername(event){
        event.preventDefault();

        const putUser = {
            username: newUsername,
            password: fullUser.password,
            person_id: fullUser.person_id,
            role_id: fullUser.role_id
        }


        fetch(baseURL + "user/" + username, {
            method: 'PUT', 
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(putUser)
        })
            .then(res => res.status).then(function(status){
                if (status === 202){
                    props.turnOff();
                    alert("User updated!")
                } else if (status === 401) {
                    alert("You don't have access to do this!")
                } else {
                    alert("Sorry, something went wrong!")
                }
            });
    }

    function submitChanges(event){
        event.preventDefault();

        const putUser = {
            id: user.id,
            firstname: newFirstName,
            lastname: newLastName,
            email: newEmail,
            gender: newGender,
            optional_id: user.optional_id
        }


        fetch(baseURL + "person/" + user.id, {
            method: 'PUT', 
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(putUser)
        })
            .then(res => res.status).then(function(status){
                if (status === 202){
                    props.turnOff();
                    alert("User updated!")
                } else if (status === 401) {
                    alert("You don't have access to do this!")
                } else {
                    alert("Sorry, something went wrong!")
                }
            });


    }

    function changeFirstName(event){
        setNewFirstName(event.target.value);
    }

    function changeLastName(event){
        setNewLastName(event.target.value);
    }

    function changeEmail(event){
        setNewEmail(event.target.value);
    }

    function changeGender(event){
        setNewGender(event.target.value);
    }



    return (
        <div>
            <Form onSubmit = {submitUsername}>
                <FormGroup>
                    <FormLabel>Username</FormLabel>
                    <FormControl required size= "sm" id = "newName" value={newUsername} onChange={changeUsername} />
                </FormGroup>
                <Button variant="secondary" type="submit" id = "button-color">Update username</Button>
            </Form>

            <Form onSubmit = {submitChanges}>
                <FormGroup>
                    <FormLabel>First name</FormLabel>
                    <FormControl required size= "sm" id = "" value={newFirstName} onChange={changeFirstName}/>

                    <FormLabel>Last name</FormLabel>
                    <FormControl required size= "sm" id = "" value={newLastName} onChange={changeLastName}/>

                    <FormLabel>Email</FormLabel>
                    <FormControl required size= "sm" id = "" value={newEmail} onChange={changeEmail}/>

                    <FormLabel>Gender</FormLabel>
                    <FormControl required size= "sm" id = "" value={newGender} onChange={changeGender}/>

                </FormGroup>
                <Button variant="secondary" type="submit" id = "button-color">Update user info</Button>
            </Form>
        </div>
    )
}


export default UpdateUserForm;