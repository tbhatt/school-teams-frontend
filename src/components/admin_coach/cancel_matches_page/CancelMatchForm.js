import React, { useState } from 'react';
import { Button, Card, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import * as URL from '../../shared/Const.js';
import '../manage_locations_page/Location.css';

function CancelMatchForm(props){
    const match = props.match;
    const history = useHistory();
    let baseURL;
    if (sessionStorage.getItem("role") === "1"){
        baseURL = URL.ADMIN_URL + "match/" + match.id;
    } else if (sessionStorage.getItem("role") === "2"){
        baseURL = URL.COACH_URL + "matches/cancel/" + match.match_id;
    }

    const [toggle, setToggle] = useState(false);
    const [reason, setReason] = useState();

    function toggleForm(){
        if (toggle){
            setToggle(false);
        } else {
            setToggle(true);
        }
    }

    function changeReason(event){
        setReason(event.target.value);
    }

    function submitCancelation(event){
        event.preventDefault();
        let cancelMatch;
        if (sessionStorage.getItem("role") === "1"){
            cancelMatch = {
                id: match.id,
                start_time: match.start_time,
                result: match.result,
                cancelled: true,
                comment: reason,
                location_id: match.location_id,
                team_1_id: match.team_1_id,
                team_2_id: match.team_2_id  
            }
        } else if (sessionStorage.getItem("role") === "2"){
            cancelMatch = {
                comment: reason,
            }
        }

        console.log(cancelMatch);

         fetch((baseURL), {
            method: 'PUT',
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(cancelMatch)
        })
        .then(function(res){
            console.log(res.status)
            return res.status
        }).then(function(status){
            if (status === 202 || status === 201){
                alert("Match cancelled!")
            } else if (status === 401) {
                alert("You don't have access to do this!")
                history.pushState("/home")
            } else {
                alert("Something went wrong!")
            }
            setToggle(false);
            return;
        })
    }
    
    return (
        <div>
            {!toggle && <Button variant="dark" onClick = {toggleForm}>CANCEL</Button>}
            {toggle && <Button variant="dark" onClick = {toggleForm}>GO BACK</Button>}

            {toggle && 
                <Card>
                    <Form onSubmit = {submitCancelation}>
                        <FormGroup>
                            <FormLabel>Reason for cancelling match on {match.start_time}</FormLabel>
                            <FormControl as="textarea" required size= "sm" id = "" placeholder="Write reason here" onChange={changeReason}/>

                        </FormGroup>
                        <Button variant="secondary" type="submit" id = "button-color">Cancel match</Button>
                    </Form>
                </Card>
            }
        </div>
    )
}

export default CancelMatchForm;