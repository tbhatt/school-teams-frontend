import React, { useEffect, useState } from 'react';

import CancelMatchesForm from './CancelMatchForm.js';
import * as URL from '../../shared/Const.js';
import '../manage_locations_page/Location.css';
import { Card, ListGroup } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

function CancelOptions(){
    let matchURL;
    let teamURL;
    if (sessionStorage.getItem("role") === "1"){
        matchURL = URL.ADMIN_URL + "match";
        teamURL = URL.ADMIN_URL + "team"
    } else if (sessionStorage.getItem("role") === "2"){
        matchURL = URL.COACH_URL + "matches";
        teamURL = URL.COACH_URL + "team"
    }

    const [matches, setMatches] = useState([]);
    const [teams, setTeams] = useState([]);
    const [chosenMatch, setChosenMatch] = useState();
    const [chosen, setChosen] = useState(false);
    const history = useHistory;

    useEffect(() => {
        fetch(matchURL,{
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
        })
        .then(function(res){
            if (res.status === 401) {
                alert("You don't have access to do this!")
                history.pushState("/home")
            } 
            return res.json()
        }).then(function(data){
            setMatches(data)
            return fetch (teamURL, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            });
        }).then(function(res){
            return res.json()
        }).then(function(data){
            setTeams(data)
            return;
        })
    }, []);

    function singleMatchClicked(event, match){
        setChosenMatch(match)
        setChosen(true);
    }

    const matchesJSX = matches.map(match => {
        if (!match.cancelled){
            let matchTeam1;
            let matchTeam2;
            if (sessionStorage.getItem("role") === "1"){
                teams.forEach(team => {
                    if (team.id === match.team_1_id){
                        matchTeam1 = team.name;
                    } else if (team.id === match.team_2_id){
                        matchTeam2 = team.name;
                    }
                });
                return(
                    <ListGroup.Item id ="half" onClick={(e) => singleMatchClicked(this, match)} key={match.id}>{matchTeam1} vs {matchTeam2} Time: {match.start_time}</ListGroup.Item>
                )
            } else if (sessionStorage.getItem("role") === "2") {
                matchTeam1 = match.home_team;
                matchTeam2 = match.away_team;
                return(
                    <ListGroup.Item id ="half" onClick={(e) => singleMatchClicked(this, match)} key={match.match_id}>{matchTeam1} vs {matchTeam2} Time: {match.start_time}</ListGroup.Item>
                )
            } 

        } else {
            return (<div id = "alreadyCancelled"></div>);
        }
    })
    return (
        <div>
            <h1 id = "white">Matches to cancel</h1>
            <Card id = "locations-list">
                {matchesJSX}
            </Card>
            {chosen && <CancelMatchesForm match = {chosenMatch}></CancelMatchesForm>}
        </div>
    )
}

export default CancelOptions;