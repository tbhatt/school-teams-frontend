import React from 'react';
import CancelOptions from './CancelOptions';
import '../manage_locations_page/Location.css';
import { Container } from 'react-bootstrap';

function CancelPage(){

    return (
        <div id = "locationBG">
            <Container>
                <CancelOptions></CancelOptions>

            </Container>
        </div>
    )
}

export default CancelPage;