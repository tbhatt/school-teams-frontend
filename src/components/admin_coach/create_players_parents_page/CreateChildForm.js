import React, { useEffect, useState } from 'react';
import { Button, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import * as URL from '../../shared/Const.js'

import './CreateUsers.css'

function CreateChildForm(){
    const baseURL = URL.REGISTER_URL;
    let parentURL;
    if (sessionStorage.getItem("role") === "1"){
        parentURL = URL.ADMIN_URL + "user/parent"
    } else {
        parentURL = URL.COACH_URL + "user/parents";
    }

    const emailURL = URL.EMAIL_URL;

    const [twoParents, setTwoParents] = useState(false);
    const [parents, setParents] = useState([]);

    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [firstParent, setFirstParent] = useState("");
    const [secondParent, setSecondParent] = useState("");
    const userType = 4;

    useEffect(() => {
        if (parents.length == 0){
            getAllParents();
        }
    })

    function getAllParents(){
        fetch(parentURL, {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
        }).then(function(response){
            return response.json();
        }).then(function(data){
            setParents(data);
            return;
        })
    }

    function toggleParentAmount(event){
        if (event.target.value === "1"){
            setTwoParents(false);
        } else {
            setTwoParents(true);
        }
    }

    const parentOptions = parents.map(parent => {
        return (
            <option key = {parent.username} value = {parent.username}>{parent.username}</option>
        )
    })


    function changeUsername(event){
        setUsername(event.target.value);
    }
    
    function changeEmail(event){
        setEmail(event.target.value)
    }

    
    function setParentOne(event){
        setFirstParent(event.target.value)
    }

    
    function setParentTwo(event){
        setSecondParent(event.target.value)
    }

    
    function submitNewPlayer(){
        const newPlayer = {
            username: username,
            password: Math.random().toString(36).substr(2, 10),
            emailAddress: email,
            role_id: userType,
            invite: false
        }

        console.log(newPlayer);

        if (firstParent.length !== 0){
            if (firstParent !== "none"){
                newPlayer.parent_1 = firstParent;
            }
        }
        if (secondParent.length !== 0){
            if (secondParent !== "none"){
                newPlayer.parent_2 = secondParent;
            }
            if (newPlayer.parent_1 === newPlayer.parent_2){
                alert("Can't have one user as two parents!")
                return;
            }
        }

        fetch(baseURL + "register", {
            method: 'POST',
            headers: {'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(newPlayer)
        }).then(function(response){
            return response.status
        }).then(function(status){
            if (status === 201 || status === 200){
                const emailBody = {
                    username: username
                }
                return fetch(emailURL + "invite", {
                    method: "POST",
                    headers: {'Content-type': 'application/json; charset=UTF-8'},
                    body: JSON.stringify(emailBody)
                }).then(function(emailRes){
                    return emailRes.status
                }).then(function(data){
                    if ((status === 201 || status === 200) && (data === 200 || data === 201)){
                        alert("New player created and invited!")
                    } else {
                        alert("Player created, but no email was sent. Please try to invite later!")
                    }
                })
            }else {
                alert("Something went wrong!");
                return;
            }
        })
    }

    return (
        <div>
            <Form>
                <FormGroup>
                    <FormLabel>Username</FormLabel>
                    <FormControl required type="text" onChange = {changeUsername}></FormControl>

                    <FormLabel>Email</FormLabel>
                    <FormControl required type = "email" onChange = {changeEmail}></FormControl>

                    <FormLabel>1 or 2 parents</FormLabel>
                    <FormControl required as = "select" onChange = {toggleParentAmount}>
                        <option key = "1" value = '1'>1</option>
                        <option key = "2" value = '2'>2</option>
                    </FormControl>

                    <FormLabel>Parent</FormLabel>
                    <FormControl as = "select" onChange = {setParentOne}>
                        <option value = "none" key = "0">No parent</option>
                        {parentOptions}
                    </FormControl>

                    {twoParents && <FormLabel>Parent</FormLabel>}
                    {twoParents && <FormControl as = "select" onChange = {setParentTwo}>
                        <option value = "none" key = "0">No parent</option>
                        {parentOptions}
                    </FormControl>}
                </FormGroup>
                <Button variant = "dark" onClick = {submitNewPlayer}>CREATE PLAYER</Button>

            </Form>

        </div>
    )
}

export default CreateChildForm;