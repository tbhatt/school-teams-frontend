import React, { useState } from 'react';
import { Button, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import * as URL from '../../shared/Const.js'

import './CreateUsers.css'

function CreateCoachForm(){
    const baseURL = URL.REGISTER_URL;
    const emailURL = URL.EMAIL_URL;
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const userType = 2;


    function changeUsername(event){
        setUsername(event.target.value);
    }
    
    function changeEmail(event){
        setEmail(event.target.value)
    }

    function submitNewCoach(){
        const newCoach = {
            username: username,
            password: Math.random().toString(36).substr(2, 10),
            emailAddress: email,
            role_id: userType,
            invite: false
        }

        fetch(baseURL + "register", {
            method: 'POST',
            headers: {'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(newCoach)
        }).then(function(response){
            return response.status
        }).then(function(status){
            if (status === 201 || status === 200){
                const emailBody = {
                    username: username
                }
                return fetch(emailURL + "invite", {
                    method: "POST",
                    headers: {'Content-type': 'application/json; charset=UTF-8'},
                    body: JSON.stringify(emailBody)
                }).then(function(emailRes){
                    return emailRes.status
                }).then(function(data){
                    if ((status === 201 || status === 200) && (data === 200 || data === 201)){
                        alert("New coach created and invited!")
                    } else {
                        alert("Coach created, but no email was sent. Please try to invite later!")
                    }
                })
            }else {
                alert("Something went wrong!");
                return;
            }
        })
    }

    return (
        <div>
            <Form>
            <FormGroup>
                    <FormLabel>Username</FormLabel>
                    <FormControl required type="text" onChange = {changeUsername}></FormControl>

                    <FormLabel>Email</FormLabel>
                    <FormControl required type = "email" onChange = {changeEmail}></FormControl>
                </FormGroup>
                <Button variant = "dark" onClick = {submitNewCoach}>CREATE COACH</Button>
            </Form>

        </div>
    )
}

export default CreateCoachForm;