import React, { useState } from 'react';
import { Button, Card, Form, FormControl, FormLabel } from 'react-bootstrap';
import CreateChildForm from './CreateChildForm.js';
import CreateParentForm from './CreateParentForm.js';
import CreateCoachForm from './CreateCoachForm.js';
import CreateAdminForm from './CreateAdminForm.js';
import * as URL from '../../shared/Const.js';
import UpdateUserForm from '../../admin/update_user_page/UpdateUserForm.js';

import './CreateUsers.css'
import { useHistory } from 'react-router-dom';

function CreateUsersPage(){
    const userType = sessionStorage.getItem("role");

    const [showUserSearch, setShowUserSearch] = useState(false);
    const [toBeSearchedFor, setToBeSearchedFor] = useState("");
    const [allUsers, setAllUsers]= useState([]);
    const [searchResults, setSearchResults] = useState([]);
    const [searchOptions, setSearchOptions] = useState();
    const [showUpdateForm, setShowUpdateForm] = useState(false);
    const history = useHistory();

    const [user, setUser] = useState();
    const [person, setPerson] = useState();

    let userOptions;


    function getAllUsers(){
        fetch(URL.ADMIN_URL + "user", {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'} 
        }).then(function(response){
         if (response.status === 401) {
                alert("You don't have access to do this!")
                history.pushState("/home")
            }
            return response.json()
        }).then(function(data){
            setAllUsers(data);
            return;
        })
    }

    if (userType === "1" ){
        userOptions= (
            <FormControl onChange={toggleView} as = "select">
                <option value = "3">Parent</option>
                <option value = "4">Player</option>
                <option value = "1">Admin</option>
                <option value = "2">Coach</option>
            </FormControl>
        )
    } else if (userType === "2"){
        userOptions= (
            <FormControl onChange= {toggleView} as = "select">
                <option value = "3">Parent</option>
                <option value = "4">Player</option>
            </FormControl>
        )
    }


    const [createParent, setCreateParent] = useState(true);
    const [createChild, setCreateChild] = useState(false);
    const [createCoach, setCreateCoach] = useState(false);
    const [createAdmin, setCreateAdmin] = useState(false);


    function toggleView(event){
        const userChoice = event.target.value;
        if (userChoice === "1"){
            setCreateAdmin(true);
            setCreateChild(false);
            setCreateCoach(false);
            setCreateParent(false);
        } else if (userChoice === "2"){
            setCreateAdmin(false);
            setCreateChild(false);
            setCreateCoach(true);
            setCreateParent(false);
        } else if (userChoice === "3"){
            setCreateAdmin(false);
            setCreateChild(false);
            setCreateCoach(false);
            setCreateParent(true);
        } else if (userChoice === "4"){
            setCreateAdmin(false);
            setCreateChild(true);
            setCreateCoach(false);
            setCreateParent(false);
        }
    }

    function openUpdateList(){
        if (showUserSearch){
            setShowUserSearch(false);
            setShowUpdateForm(false);
        } else {
            getAllUsers();
            setShowUserSearch(true);
            setCreateParent(false);
            setCreateCoach(false);
            setCreateAdmin(false);
            setCreateChild(false);
            searchFor("");
        }
    }

    function searchFor(searchTerm){
        const resultList = allUsers.filter(user => {
            return user.username.toLowerCase().includes(searchTerm.toLowerCase())
        })
        setSearchResults(resultList);
        const jsxResults = resultList.map(user => {
            return (
                <option key={user.username} value = {user.username}>{user.username}</option>
            )
        })
        setSearchOptions(jsxResults);

    }

    function updateSearch(event){
        setToBeSearchedFor(event.target.value);
        searchFor(event.target.value);
    }

    function setUserId(event){
        console.log(event.target.value);
        const username = event.target.value;
        fetch(URL.ADMIN_URL + "user/" + username, {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'} 
        }).then(function(response){
            return response.json()
        }).then(function(data){
            setUser(data);
            return fetch(URL.ADMIN_URL + "person/" + data.person_id, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'} 
            })
        }).then(function(res){
            return res.json()
        }).then(function(persona){
            setPerson(persona)
            return;
        }).then(function(){
            setShowUpdateForm(true);
            return;
        })
    }

    function turnOff(){
        setShowUpdateForm(false);
    }


    return (
        <div id = "backgroundPage">
            {(userType === "1" && showUserSearch) && <Card id = "updateCard">
                <Card.Body id  = "flex">
                    {showUpdateForm && <UpdateUserForm turnOff = {turnOff} fullUser = {user} user = {person} username = {user.username}></UpdateUserForm>}
                    
                    {(showUserSearch && !showUpdateForm) &&
                    <Form>
                        <FormLabel>Username:</FormLabel>
                        <FormControl type = "text" onChange={updateSearch}></FormControl>
                    </Form>
                    }

                    {(showUserSearch && !showUpdateForm) &&
                    <Form>
                        <FormLabel>User to update</FormLabel>
                        <FormControl as="select" onChange={setUserId}>
                            {searchOptions}
                        </FormControl>
                    </Form>
                    }      
                    {(userType === "1" && showUserSearch) && <Button variant = "secondary" id = "buttonRight" onClick = {openUpdateList}>Go back</Button>}

                </Card.Body>
            </Card>}

            {!showUserSearch && <Card id = "addCard">
                <Card.Body id = "flex">
                    {!showUserSearch && <Form>
                        <FormLabel>User type</FormLabel>
                        {userOptions}
                    </Form>}

                    {createParent && <CreateParentForm></CreateParentForm>}
                    {createChild && <CreateChildForm></CreateChildForm>}
                    {createCoach && <CreateCoachForm></CreateCoachForm>}
                    {createAdmin && <CreateAdminForm></CreateAdminForm>}
                    
                    {(userType === "1" && !showUserSearch) && <Button variant = "secondary" onClick = {openUpdateList} id = "buttonRight">Update users</Button>}

                </Card.Body>
            </Card>}

        </div>
    )
}


export default CreateUsersPage;