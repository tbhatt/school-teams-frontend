import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap';
import './Location.css';

const EditLocation = (props) => {
    //True = add, False = Edit
    //const add = true;
    
    const[name, setName] = useState("");
    const[street, setStreet] = useState("");
    const[number, setNumber] = useState("");
    const[postCode, setPostCode] = useState("");
    const[postCity, setPostCity] = useState("");
    const[location, setLocation] = useState(props.location);
    const[lock, setLock] = useState(true);
    const[hide, setHide] = useState(true); //Hide buttons

    useEffect(() => { 
        setLocation(props.location);
        setName(props.location.name);
        setStreet(props.location.street);
        setNumber(props.location.number);
        setPostCode(props.location.postalCode_post_code);
        setPostCity(props.location.postal_city);

        if(props.operation) {
            setLock(false);
            setHide(false);
        }
        else{
            setLock(true);
            setHide(true);
        }

    }, [props.location]);

    const submit = () => {
        let result = {
            street: street,
            number: number,
            name: name,
            postalCode_post_code: postCode
        }
        if(!lock) {
            result.id = props.location.id
        }
        props.setLocationInfo(result);
    }

    const unlock = () => {
        setLock(!lock);
    }
    
    return (
        <form><fieldset disabled={lock}>
            <div class="form-row">
                <div class="col">
                    <input type="text" class="form-control" placeholder="Name"
                     onChange={e => setName(e.target.value)}
                     value={name}
                    />
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <input type="text"  class="form-control" placeholder="Street name"
                     onChange={e => setStreet(e.target.value)}
                     value={street}
                     />
                </div>
                <div class="col">
                    <input type="number" class="form-control" placeholder="Street number"
                     onChange={e => setNumber(e.target.value)}
                     value={number}/>
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <input type="number" class="form-control" placeholder="Zip code"
                     onChange={e => setPostCode(e.target.value)}
                     value={postCode}
                    />
                </div>
                <div class="col">
                    <input type="text" class="form-control" placeholder="Place"
                     onChange={e => setPostCity(e.target.value)}
                     value={postCity}
                     disabled="true"
                     />
                </div>
            </div>

            <div class="form-row">
                <div class="col">
                    <Button onClick={submit} variant="outline-dark">
                        Submit
                    </Button>  
                </div>
                <div class="col">
                    {hide && <Button hide="true" onClick={submit} variant="outline-dark">
                        Delete
                    </Button>}   
                </div>
            </div>
      
            </fieldset>
            <div class="form-row">
                <div class="col">
                    {hide && <Button onClick={unlock} id = "bottom" variant="outline-dark">
                    Edit
                    </Button>}
                </div>
                <div class="col">
                    <Button onClick={props.addLocationClicked}  variant="dark">
                        Add new location
                    </Button>  
                </div>
            </div>
            
                

            </form>
    )
}
export default EditLocation
