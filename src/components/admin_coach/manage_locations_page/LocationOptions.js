import Axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Button, Card, ListGroup } from 'react-bootstrap';
import EditLocation from './EditLocation'
import LocationView from './LocationView';
import './Location.css';

import * as URL from '../../shared/Const.js';
import { useHistory } from 'react-router-dom';

export const LocationOptions = () => {
    let baseURL;
    if (sessionStorage.getItem("role") === "1"){
        baseURL = URL.ADMIN_URL;
    } else if (sessionStorage.getItem("role") === "2"){
        baseURL = URL.COACH_URL;
    }
    const [locations, setLocations] = useState(); //All locations 
    const [location, setLocation] = useState(); //Selected location
    const [loading, setLoading] = useState(true); //Loading
    const [operation, setOperation] = useState(false); //True==Add, False==Edit
    const history = useHistory();
    const newLocation = {
        name: "",
        street: "",
        number: 0,
        postalCode_post_code: 0,
        postal_city:""
    }

    useEffect(() => {
        getLocations();
    }, [])
    
    const getLocations = () => { //Gets all locations
        let newUrl;
        if (sessionStorage.getItem("role") === "1"){
            newUrl = baseURL + "location/full";
        } else {
            newUrl = baseURL + "location"
        }
        console.log("Getting locations")
        Axios.get(newUrl, 
        {headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},})
            .then( response => {
                if (response.status === 401) {
                    alert("You don't have access to do this!")
                    history.pushState("/home")
                }
                setLocations(response.data);
                setLocation(response.data[0]);
                setLoading(false);
            })
    }

    const setLocationInfo = async (result) => {
        console.log(JSON.stringify(result));
        if(operation){ //Add new location
            console.log("Adding new locations")
            await Axios.post(baseURL + 'location', result, 
            {headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}})

        }
        else{ //Edit location
            console.log("Updating location with ID: " + result.id)
            let newUrl;
            if (sessionStorage.getItem("role") === "1"){
                newUrl = baseURL + "location/";
            } else {
                newUrl = baseURL + "location/update/"
            }
            await Axios.put(newUrl + `${result.id}`, result,
            {headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}})
                .then(res => {
                    console.log(res);
                })
        }
        getLocations();
        
    }

    const locationClicked = (locationInfo) => { 
        console.log("Location clicked");
        console.log(locationInfo);
        setLocation(locationInfo);
        setOperation(false);
    }

    const addLocationClicked = () => {
        console.log("Clicked add a new location")
        setLocation(newLocation);
        setOperation(true);
    }

    if(loading){
        return <div>Loading...</div>
    }

    return (
        <div class="container">
            <h1 id = "whiteFont">Manage locations</h1>
            <Card>
                <h3>Location details</h3>
                <EditLocation addLocationClicked={addLocationClicked} location={location} operation={operation} setLocationInfo={setLocationInfo}></EditLocation>                
            
                
            </Card>

            <h1 id = "whiteFont">Click a location to manage it </h1>
            <Card id="locations-list">
                {locations.map(location => <LocationView key={location.id} location={location} locationClick={locationClicked}></LocationView>)}


            </Card>

            
        </div>
    )
}
export default LocationOptions
