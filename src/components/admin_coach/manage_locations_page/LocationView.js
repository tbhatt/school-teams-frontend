import React from 'react'
import {ListGroup } from 'react-bootstrap';
import './Location.css';

export const LocationView = (props) => {
    const location = props.location;

    const locationSelected = () => {
        props.locationClick(location);
    }

    return (
        <ListGroup.Item onClick={locationSelected} id = "half">
            Name: {location.name}            
        </ListGroup.Item>
    )
    
}
export default LocationView
