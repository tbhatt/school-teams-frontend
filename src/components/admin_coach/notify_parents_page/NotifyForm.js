import React, { useState } from 'react';
import { Button, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import * as URL from '../../shared/Const.js';


function NotifyForm(props){
    let baseURL;
    if (sessionStorage.getItem("role") === "1"){
        baseURL = URL.ADMIN_URL;
    } else if (sessionStorage.getItem("role") === "2"){
        baseURL = URL.COACH_URL;
    }


    const [toggle, setToggle] = useState("");
    const [message, setMessage] = useState("");
    const [user, setUser] = useState(sessionStorage.getItem("user"));
    const [school, setSchoolId] = useState("");
    const [team, setTeamId] = useState("");

    const [showSchool, setShowSchool] = useState(false);
    const [showTeam, setShowTeam] = useState(false);

    function toggleGroup(event){
        setToggle(event.target.value);
        if (toggle === "school"){
            setShowSchool(false);
            setSchoolId("");
            setShowTeam(true);
        } else {
            setShowSchool(true);
            setTeamId("");
            setShowTeam(false);
        }
    }

    function submitNotification(event){
        event.preventDefault();
        const POSTrequest = {
                message: message,
                sender: user
        }
        if (school !== ""){
            fetch((baseURL + "user/notify/school/" + school), {
                method: 'POST',
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},                
                body: JSON.stringify(POSTrequest)
            }).then(function(res){
                console.log(res.status)
                return res.status
            }).then(function(status){
                if (status === 200){
                    alert("Notification sent!")
                } else if (status === 204) {
                    alert("This school doesn't have any teams!")
                } else {
                    alert("Something went wrong!")
                }
                return;
            })
        } else if (team !== ""){
            fetch((baseURL + "user/notify/team/" + team), {
                method: 'POST',
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},                body: JSON.stringify(POSTrequest)
            }).then(function(res){
                console.log(res.status)
                return res.status
            }).then(function(status){
                if (status === 200){
                    alert("Notification sent!")
                } else {
                    alert("Something went wrong!")
                }
                return;
            })
        }
    }

    function changeMessage(event){
        setMessage(event.target.value);
    }


    function changeSchoolId(event){
        setSchoolId(event.target.value);
    }

    function changeTeamId(event){
        setTeamId(event.target.value);
    }

    return (
        <div>
            <Form onSubmit = {submitNotification}>
                <FormGroup>
                    <FormLabel>Message</FormLabel>
                    <FormControl as="textarea" required size= "sm" id = "teamname" placeholder="The message you want to send" onChange={changeMessage} />
                

                    <FormLabel>Notification Groups</FormLabel>
                    <FormControl required as="select" onChange = {toggleGroup}>
                        <option key = "team" value = "team">Teams</option>
                        <option key = "school" value = "school">Schools</option>
                    </FormControl>

                    {showSchool && <FormLabel>School</FormLabel>}
                    {showSchool && <FormControl required as="select" onChange = {changeSchoolId}>
                        {props.schools}
                    </FormControl>}

                    {showTeam && <FormLabel>Team</FormLabel>}
                    {showTeam && <FormControl required as="select" onChange = {changeTeamId}>
                        {props.teams}
                    </FormControl>}

                </FormGroup>
                <Button variant="secondary" type="submit" id = "button-color">Send notification</Button>
            </Form>
        </div>
    )
}

export default NotifyForm;