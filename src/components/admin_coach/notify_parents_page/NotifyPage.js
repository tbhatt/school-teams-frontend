import React, { useEffect, useState } from 'react';
import { Card, Container } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import * as URL from '../../shared/Const.js';
import '../manage_locations_page/Location.css';
import NotifyForm from './NotifyForm.js';

function NotifyPage(){
    let baseURL;
    if (sessionStorage.getItem("role") === "1"){
        baseURL = URL.ADMIN_URL;
    } else if (sessionStorage.getItem("role") === "2"){
        baseURL = URL.COACH_URL;
    } 
    const history = useHistory();
    const [teams, setTeams] = useState([]);
    const [schools, setSchools] = useState([]);


    useEffect(() => {
        fetch(baseURL + "team", {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
        })
        .then(function(res){
            if (res.status === 401) {
                alert("You don't have access to do this!")
                history.pushState("/home")
            }
            return res.json()
        }).then(function(data){
            setTeams(data)
            return fetch(baseURL + "school", {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            })
        }).then(function(res){
                return res.json()
            }).then(function(data){
                setSchools(data);
                return 
            })
            }, []
    );
    
    const schoolOptions = schools.map(school => {
        return (
            <option key = {school.id} value = {school.id}>{school.name}</option>
        )
    })

    const teamOptions = teams.map(team => {
        return (
            <option key = {team.id} value = {team.id}>{team.name}</option>
        )
    })

    return (
        <div id = "locationBG">
            <Container >
                <h1 id ="white">Message parents</h1>

                <Card id ="marginTop">
                    <NotifyForm schools = {schoolOptions} teams = {teamOptions}></NotifyForm>

                </Card>

            </Container>
        </div>
    )
}

export default NotifyPage;