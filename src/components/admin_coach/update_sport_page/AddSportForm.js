import React, { useState } from 'react';
import { Button, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import * as URL from '../../shared/Const.js';

function AddSportForm(props){
    let sportUrl;
    const history = useHistory();
    if (sessionStorage.getItem("role") === "1"){
        sportUrl = URL.ADMIN_URL + "sport/";
    } else if (sessionStorage.getItem("role") === "2"){
        sportUrl = URL.COACH_URL + "sport/add/";
    }

    const [sportName, setSportName] = useState("");

    function changeSportname(event){
        setSportName(event.target.value);
    }

    function submitNewSport(event){
        event.preventDefault();

        const newSport = {
            name: sportName
        }

        fetch (sportUrl, {
            method: 'POST',
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},            body: JSON.stringify(newSport)
        }).then(function(res){
            return res.status
        }).then(function(status){
            if (status === 201){
                alert("New sport created!");
                props.toggleOff();
            } else if (status === 401) {
                alert("You don't have access to do this!")
                history.pushState("/home")
            } else {
                alert("Something went wrong!");
                props.toggleOff();
            }
        }).catch(function(error){
            console.log("failed: " + error)
        })
    }

    return(
        <div>
            <Form onSubmit = {submitNewSport}>
                <FormGroup>
                    <FormLabel>Sport</FormLabel>
                    <FormControl required size= "sm" id = "sportname" placeholder="Name of the sport" onChange={changeSportname} />
                </FormGroup>
                <Button variant="secondary" type="submit" id = "button-color">Add sport</Button>
            </Form>

        </div>
    )
}

export default AddSportForm;