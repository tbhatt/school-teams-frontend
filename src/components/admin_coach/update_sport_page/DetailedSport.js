import React, { useState } from 'react';
import UpdateSportForm from './UpdateSportForm.js';
import * as URL from '../../shared/Const.js';
import '../manage_locations_page/Location.css'
import { Button } from 'react-bootstrap';

function DetailedSport(props){
    let sportname = props.sportName;
    const sport = props.sport;
    const [doUpdate, setDoUpdate] = useState(false);

    let baseURL;
    if (sessionStorage.getItem("role") === "1"){
        baseURL = URL.ADMIN_URL + "sport/";
    } else if (sessionStorage.getItem("role") === "2"){
        baseURL = URL.COACH_URL + "sport/";
    }


    function deleteSport(event){
        if (sportname !== undefined){
            const specificSportURL = baseURL + sport.id;
            console.log("deleted");
            fetch(specificSportURL, {method: 'DELETE', 
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}})
            .then(res => res.json());

        } else {
            console.log("no sport chosen");
        }
    }

    function toggleUpdate(event){
        if (doUpdate){
            props.turnOffAdd();
            setDoUpdate(false)
        } else {
            if (sportname !== undefined){
                setDoUpdate(true)
                props.turnOffAdd();
            }
        }
    }

    return (
        <div>
            {(!doUpdate && sportname !== undefined) && <h6>Sport to edit: {sportname}</h6>}
            {doUpdate && <UpdateSportForm sport = {sport} id={sport.id}></UpdateSportForm>}
            <div>
                {!doUpdate && <Button variant="outline-dark" onClick={toggleUpdate}>UPDATE</Button>}
                {doUpdate && <Button variant="outline-dark" onClick={toggleUpdate}>GO BACK</Button>}
            
                <Button variant="outline-dark" onClick={deleteSport}>DELETE</Button>
            </div>

        </div>
    )
}

export default DetailedSport;