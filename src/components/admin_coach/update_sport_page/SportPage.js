import React, { useEffect, useState } from 'react';

import DetailedSport from './DetailedSport.js';
import AddSportForm from './AddSportForm.js';

import * as URL from '../../shared/Const.js';
import { Button, Card, Container, ListGroup } from 'react-bootstrap';
import '../manage_locations_page/Location.css'


function SportPage(){

    let sportUrl;
    if (sessionStorage.getItem("role") === "1"){
        sportUrl = URL.ADMIN_URL + "sport/";
    } else if (sessionStorage.getItem("role") === "2"){
        sportUrl = URL.COACH_URL + "sport/";
    }

    const [sports, setSports] = useState([]); 
    const [chosenSport, setChosenSport] = useState("");
    const [toggleAddSport, setToggleAddSport] = useState(true);

    useEffect(() => {
        fetch(sportUrl, {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
        })
            .then((res) => res.json())
                .then( res => setSports(res))
            }, []
    );

    function singleSportClicked(event, sport){
        setChosenSport(sport);
    }

    const toggleAddButton = () => {
        if (toggleAddSport){
            setToggleAddSport(false)
        } else {
            setToggleAddSport(true)
        }
    }
    
    const sportOptions = sports.map(sport => {
        return (
            <ListGroup.Item id = "half" key = {sport.id} onClick={(e) => singleSportClicked(this, sport)}>{sport.name}</ListGroup.Item>
        )
    })



    return (
        <Container>
            <h1 id ="white">Sports</h1>
            <div>

            <Card>
                <h3>Sport details</h3>
                <DetailedSport turnOffAdd={toggleAddButton} sport = {chosenSport} sportName = {chosenSport.name} sportId = {chosenSport.id}></DetailedSport>
                {toggleAddSport && <AddSportForm toggleOff={toggleAddButton}></AddSportForm>}


            </Card>
            </div>

            <div>
            <h1 id = "whiteFont">Click a school to manage it </h1>

            <Card id = "locations-list">
                {sportOptions}
            </Card>
            </div>

        </Container>
    )
}

export default SportPage;