import React, { useState } from 'react';
import { Button, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import * as URL from '../../shared/Const.js';
import '../manage_locations_page/Location.css'

function UpdateSportForm(props){
    const sport = props.sport;

    let baseURL;
    if (sessionStorage.getItem("role") === "1"){
        baseURL = URL.ADMIN_URL + "sport/" + props.id;
    } else if (sessionStorage.getItem("role") === "2"){
        baseURL = URL.COACH_URL + "sport/update/" + props.id;
    }
    const [newSportname, setNewSportname] = useState(props.name);

    function changeSportname(event){
        setNewSportname(event.target.value)
    }

    function submitSportname(event){
        event.preventDefault();
        const putSport = {
            id: sport.id,
            name: newSportname
        }

        fetch(baseURL, {
            method: 'PUT', 
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(putSport)
        })
            .then(res => res.status).then(function(status){
                if (status ===202){
                    alert("Sport updated!!")
                } else if (status === 401){
                    alert("You don't have permission to do that!")
                }
            });

    }

    return (
        <div>
            <Form onSubmit = {submitSportname}>
                <FormGroup>
                    <FormLabel>Sport name</FormLabel>
                    <FormControl required size= "sm" id = "newSportname" defaultValue={sport.name} onChange={changeSportname} />
                </FormGroup>
                <Button variant="secondary" type="submit" id = "button-color">Update sport</Button>
            </Form>
        </div>
    )
}

export default UpdateSportForm;