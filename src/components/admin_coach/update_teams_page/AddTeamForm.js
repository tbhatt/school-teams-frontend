import React, { useState } from 'react';
import { Button, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import * as URL from '../../shared/Const.js';
import '../manage_locations_page/Location.css'

function AddTeamForm(props){
    let baseURL;
    const history = useHistory();
    if (sessionStorage.getItem("role") === "1"){
        baseURL = URL.ADMIN_URL;
    } else if (sessionStorage.getItem("role") === "2"){
        baseURL = URL.COACH_URL;
    }

    //post request-info
    const [teamName, setTeamName] = useState();
    const [schoolId, setSchoolId] = useState();
    const [coachName, setCoachName] = useState();
    const [sportId, setSportId] = useState();

    //get request-info
    const schools = props.schools;
    const coaches = props.coaches;
    const sports = props.sports;

    function changeTeamName(event){
        setTeamName(event.target.value);
    }

    function changeSchoolId(event){
        setSchoolId(event.target.value);
    }

    function changeCoach(event){
        setCoachName(event.target.value);
    }

    function changeSportId(event){
        setSportId(event.target.value);
    }

    function submitNewTeam(event){
        event.preventDefault();
        const newTeam = {
            name: teamName,
            school_id: schoolId,
            coach: coachName,
            sport_id: sportId
        }
        
        fetch (baseURL + "team/", {
            method: 'POST',
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(newTeam)
        }).then(function(res){
            return res.status
        }).then(function(status){
            if (status === 201){
                alert("New team created!");
                props.toggleOff();
            } else if (status === 401) {
                alert("You don't have access to do this!")
                history.pushState("/home")
            } else {
                alert("Something went wrong!");
                props.toggleOff();
            }
        }).catch(function(error){
            console.log("failed: " + error)
        })
    }

    const schoolOptions = schools.map(school => {
        return (
            <option key = {school.id} value = {school.id}>{school.name}</option>
        )
    })

    const coachOptions = coaches.map(coach => {
        return (
            <option key = {coach.username} value = {coach.username}>{coach.username}</option>
        )
    })

    const sportOptions = sports.map(sport => {
        return (
            <option key = {sport.id} value = {sport.id}>{sport.name}</option>
        )
    })

    return (
        <div>
            <Form onSubmit = {submitNewTeam}>
                <FormGroup>
                    <FormLabel>Team name</FormLabel>
                    <FormControl required size= "sm" id = "teamname" placeholder="The name of the team" onChange={changeTeamName} />
                
                    <FormLabel>School</FormLabel>
                    <FormControl required as="select" onChange = {changeSchoolId}>
                        {schoolOptions}
                    </FormControl>

                    <FormLabel>Coach</FormLabel>
                    <FormControl required as="select" onChange = {changeCoach}>
                        {coachOptions}
                    </FormControl>

                    <FormLabel>Sport</FormLabel>
                    <FormControl required as="select" onChange = {changeSportId}>
                        {sportOptions}
                    </FormControl>

                </FormGroup>
                <Button variant="secondary" type="submit" id = "button-color">Add team</Button>
            </Form>
        </div>
    )
}

export default AddTeamForm;