import React, { useEffect, useState } from 'react';
import DetailedTeam from '../../admin_coach/update_teams_page/DetailedTeam.js';
import * as URL from '../../shared/Const.js';
import { Button, Card, Container, Form, FormControl, FormGroup, FormLabel, ListGroup } from 'react-bootstrap';
import '../manage_locations_page/Location.css'


function CoachAddTeam(){
    const [teamName, setTeamName] = useState("");
    const [school_id, setSchool_id] = useState("");
    const [sport_id, setSport_id] = useState("");

    const [toggleAddTeam, setToggleAddTeam] = useState(true);
    const [toggleUpdateTeam, setToggleUpdateTeam] = useState(false);

    const [chosenTeam, setChosenTeam] = useState("");
    const [sports, setSports] = useState([]);
    const [schools, setSchools] = useState([]);

    const [teams, setTeams] = useState([]);

    const coaches = [{username: sessionStorage.getItem("user")}]
    
    const baseURL = URL.COACH_URL;
    
    useEffect(() => {
        fetch(baseURL + "team/coach/" + sessionStorage.getItem("user"), {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
        }).then(function(response){
            return response.json();
        }).then(function(data){
            setTeams(data);
            return fetch(baseURL + "sport", {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            }).then(function(response){
                return response.json()
            }).then(function(data){
                setSports(data);
                return fetch(baseURL + "school", {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'} 
                }).then(function(response){
                    return response.json();
                }).then(function(data){
                    setSchools(data);
                    return;
                })
            })
        })
    }, [baseURL])

    function singleTeamClicked(event, team){
        setChosenTeam(team);
        setToggleUpdateTeam(true);
    }

    function addTeam(event){
        event.preventDefault();
        const teamRequest = {
            name: teamName,
            school_id: school_id,
            coach: sessionStorage.getItem("user"),
            sport_id: sport_id
        };

        fetch (baseURL + "team/add", {
            method: 'POST',
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(teamRequest)
        }).then(function(res){
            return res.status
        }).then(function(status){
            if (status === 201){
                alert("New team created!");
                toggleAddButton();;
            } else {
                alert("Something went wrong!");
                toggleAddButton();;
            }
        }).catch(function(error){
            console.log("failed: " + error)
        })

    }


    function updateTeam(event){
        event.preventDefault();
        const teamRequest = {
            name: teamName,
            school_id: school_id,
            coach: sessionStorage.getItem("user"),
            sport_id: sport_id
        };

        fetch (baseURL + "team/update/" + chosenTeam.id, {
            method: 'PUT',
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(teamRequest)
        }).then(function(res){
            return res.status
        }).then(function(status){
            if (status === 200 || status === 201){
                alert("Team updated!!");
                toggleAddButton();;
            } else {
                alert("Something went wrong!");
                toggleAddButton();;
            }
        }).catch(function(error){
            console.log("failed: " + error)
        })
        
    }

    
    const toggleAddButton = () => {
        if (toggleAddTeam){
            setToggleAddTeam(false)
            setToggleUpdateTeam(true)
        } else {
            setToggleAddTeam(true)
            setToggleUpdateTeam(false)
        }
    }
        
    const toggleUpdateButton = () => {
        if (toggleUpdateTeam){
            setToggleUpdateTeam(false)
        } else {
            if (chosenTeam.length !== 0){
                setToggleUpdateTeam(true)
            }
        }
    }

    const teamOptions = teams.map(team => {
        return (
            <ListGroup.Item id="quarter" key = {team.id} onClick={(e) => singleTeamClicked(this, team)}>{team.name}</ListGroup.Item>
        )
    })

    function changeSchoolId(event){
        setSchool_id(event.target.value);
    }

    function changeSportId(event){
        setSport_id(event.target.value);
    }

    function changeTeamName(event){
        setTeamName(event.target.value);
    }

    const schoolOptions = schools.map(school => {
        return (
            <option key = {school.id} value = {school.id}>{school.name}</option>
        )
    })

    const sportOptions = sports.map(sport => {
        return (
            <option key = {sport.id} value = {sport.id}>{sport.name}</option>
        )
    })
    
    
    return (
        <div>
            <Container>
                <h1 id ="white">Teams</h1>
                <div>
                    <Card>
                    <h3>Team details</h3>
                    {toggleAddTeam && <Form onSubmit = {addTeam}>
                    <FormGroup>
                    <FormLabel>Team name</FormLabel>
                    <FormControl required size= "sm" id = "teamname" placeholder="The name of the team" onChange={changeTeamName} />
                
                    <FormLabel>School</FormLabel>
                    <FormControl required as="select" onChange = {changeSchoolId}>
                        {schoolOptions}
                    </FormControl>

                    <FormLabel>Sport</FormLabel>
                    <FormControl required as="select" onChange = {changeSportId}>
                        {sportOptions}
                    </FormControl>

                    </FormGroup>
                    <Button variant="dark" type="submit" id = "button-color">Add team</Button>
                    </Form>}
                    {toggleUpdateTeam && <DetailedTeam toggleAddButton={toggleAddButton} team = {chosenTeam} teamName = {chosenTeam.name} teamId = {chosenTeam.id} schools = {schools} coaches = {coaches} sports = {sports}></DetailedTeam>}






                    </Card>
                </div>
                <h3 id = "whiteFont">Click a team to manage it </h3>
                
                <Card id ="team-list">
                {teamOptions}

                </Card>


            </Container>

        </div>
    )
}

export default CoachAddTeam;