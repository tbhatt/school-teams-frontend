import React, { useEffect, useState } from 'react';

import UpdateTeamForm from './UpdateTeamForm.js';
import * as URL from '../../shared/Const.js';
import '../manage_locations_page/Location.css'
import { Button } from 'react-bootstrap';

function DetailedTeam(props){
    const team = props.team;
    const schools = props.schools;
    const coaches = props.coaches;
    const sports = props.sports;
    const [school, setSchoolName] = useState("");
    const [coach, setCoachName] = useState("");
    const [sport, setSportName] = useState("");
    const [doUpdate, setDoUpdate] = useState(false);


    useEffect(()=> {
        schools.forEach(school => {
            if (school.id === team.school_id){
                setSchoolName(school.name)
                return;
            }
        });
        coaches.forEach(coach => {
            if (coach.username === team.coach){
                setCoachName(coach.username)
                return;
            }
        });
        sports.forEach(sport => {
            if (sport.id === team.sport_id){
                setSportName(sport.name)
                return;
            }
        });
    })

    function deleteTeam(event){
        if (team !== ""){
            
            let teamURL;
            if (sessionStorage.getItem("role") === "1"){
                teamURL = URL.ADMIN_URL + "team/" + team.id;
            } else if (sessionStorage.getItem("role") === 2){
                teamURL = URL.COACH_URL + "team/delete/" + team.id;
            }
            fetch(teamURL, {method: 'DELETE', headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}})
            .then(res => res.json());
            console.log("delete")
        }
    }

    const updateTeam = () => {
        if (doUpdate){
            setDoUpdate(false)
            props.toggleAddButton()
        } else {
            if (team !== ""){
                setDoUpdate(true)
                props.toggleAddButton()
            }
        }
    }


    return (
        <div>
        {(!doUpdate && team !== undefined) && <h6>Team to edit: {team.name}</h6>}
        {doUpdate && <UpdateTeamForm team = {team} schools = {schools} coaches = {coaches} sports = {sports}></UpdateTeamForm>}
        <div>
            {!doUpdate && <Button variant="outline-dark" onClick={updateTeam}>UPDATE</Button>}
            {doUpdate && <Button variant="outline-dark" onClick={updateTeam}>GO BACK</Button>}
        
            <Button variant="outline-dark" onClick={deleteTeam}>DELETE</Button>
        </div>

    </div>

    )
}

export default DetailedTeam;