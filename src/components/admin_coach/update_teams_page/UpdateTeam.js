import React, { useEffect, useState } from 'react';

import DetailedTeam from './DetailedTeam.js';
import AddTeamForm from './AddTeamForm.js';

import * as URL from '../../shared/Const.js';
import '../manage_locations_page/Location.css'
import { Card, Container, ListGroup } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

function UpdateTeam(){
    const history = useHistory();
    let baseURL;
    if (sessionStorage.getItem("role") === "1"){
        baseURL = URL.ADMIN_URL;
    } else if (sessionStorage.getItem("role") === "2"){
        baseURL = URL.COACH_URL;
    }

    const [teams, setTeams] = useState([]); 
    const [chosenTeam, setChosenTeam] = useState("");
    const [toggleAddTeam, setToggleAddTeam] = useState(true);

    const [schools, setSchools] = useState([]);
    const [coaches, setCoaches] = useState([]);
    const [sports, setSports] = useState([]);


    useEffect(() => {
        fetch(baseURL + "team", {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
        })
        .then(function(res){
            if (res.status === 401) {
                alert("You don't have access to this page!")
                history.pushState("/home")
            }
            return res.json()
        }).then(function(data){
            setTeams(data)
            return fetch(baseURL + "school", {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            })
        }).then(function(res){
                return res.json()
            }).then(function(data){
                setSchools(data);
                return fetch(baseURL + "user/coach", {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                })
            }).then(function(res){
                return res.json()
            }).then(function(data){
                setCoaches(data)
                return fetch(baseURL + "sport", {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                })
            }).then(function(res){
                return res.json()
            }).then(function(data){
                setSports(data)
                return;
            })
            }, []
    );

    const toggleAddButton = () => {
        if (toggleAddTeam){
            setToggleAddTeam(false)
        } else {
            setToggleAddTeam(true)
        }
    }

    function singleTeamClicked(event, team){
        setChosenTeam(team);
    }

    const teamOptions = teams.map(team => {
        return (
            <ListGroup.Item id="quarter"key = {team.id} onClick={(e) => singleTeamClicked(this, team)}>{team.name}</ListGroup.Item>
        )
    })


    return (
        <Container>
            <h1 id ="white">Teams</h1>
            <div>
                <Card>
                    <h3>Team details</h3>
                    {toggleAddTeam && <AddTeamForm toggleOff={toggleAddButton} schools = {schools} coaches = {coaches} sports = {sports}></AddTeamForm>}

                    <DetailedTeam toggleAddButton={toggleAddButton} team = {chosenTeam} teamName = {chosenTeam.name} teamId = {chosenTeam.id} schools = {schools} coaches = {coaches} sports = {sports}></DetailedTeam>



                </Card>
            </div>

            <h3 id = "whiteFont">Click a team to manage it </h3>


            <Card id ="team-list">
                {teamOptions}

            </Card>

        </Container>
    )
}

export default UpdateTeam;