import React, { useState } from 'react';
import { Button, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import * as URL from '../../shared/Const.js';

function UpdateTeamForm(props){
    const schools = props.schools;
    const coaches = props.coaches;
    const sports = props.sports;
    const team = props.team;

    let baseURL;
    if (sessionStorage.getItem("role") === "1"){
        baseURL = URL.ADMIN_URL + "team/" + team.id;
    } else if (sessionStorage.getItem("role") === "2"){
        baseURL = URL.COACH_URL + "team/update/" + team.id;
    }

    const [teamName, setTeamName] = useState(props.team.name);
    const [schoolId, setSchoolId] = useState();
    const [coachName, setCoachName] = useState();
    const [sportId, setSportId] = useState();


    function changeTeamName(event){
        setTeamName(event.target.value);
    }

    function changeSchoolId(event){
        setSchoolId(event.target.value);
    }

    function changeCoach(event){
        setCoachName(event.target.value);
    }

    function changeSportId(event){
        setSportId(event.target.value);
    }


    function updateTeam(event){
        event.preventDefault();
        let newName = teamName;
        let sport = sportId;
        let school = schoolId;
        let newCoach = coachName;

        if (sportId === undefined){
            sport = team.sport_id;
        }
        if (teamName === undefined){
            newName  = team.name;
        }
        if (schoolId === undefined){
            school = team.school_id;
        }
        if (coachName === undefined){
            newCoach = team.coach;
        }

        const putTeam = {
            id: team.id,
            name: newName,
            school_id: school,
            coach: newCoach,
            sport_id: sport,
        }

        fetch(baseURL, {
            method: 'PUT', 
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(putTeam)
        })
            .then(res => res.json()).then(res => console.log(res));

    }



    const schoolOptions = schools.map(school => {
        return (
            <option key = {school.id} value = {school.id}>{school.name}</option>
        )
    })

    const coachOptions = coaches.map(coach => {
        console.log(coach);
        return (
            <option key = {coach.username} value = {coach.username}>{coach.username}</option>
        )
    })

    const sportOptions = sports.map(sport => {
        return (
            <option key = {sport.id} value = {sport.id}>{sport.name}</option>
        )
    })
    
    return (
        <div>
            <Form onSubmit = {updateTeam}>
                <FormGroup>
                    <FormLabel>Team name</FormLabel>
                    <FormControl size= "sm" id = "teamname" defaultValue={teamName} onChange={changeTeamName} />
                
                    <FormLabel>School</FormLabel>
                    <FormControl as="select" onChange = {changeSchoolId}>
                        {schoolOptions}
                    </FormControl>

                    <FormLabel>Coach</FormLabel>
                    <FormControl as="select" onChange = {changeCoach}>
                        {coachOptions}
                    </FormControl>

                    <FormLabel>Sport</FormLabel>
                    <FormControl as="select" onChange = {changeSportId}>
                        {sportOptions}
                    </FormControl>

                </FormGroup>
                <Button variant="secondary" type="submit" id = "button-color">Update team</Button>
            </Form>
        </div>
    )
}

export default UpdateTeamForm;