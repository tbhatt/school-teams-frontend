import React from 'react';
import CoachAddTeam from './CoachAddTeam.js';
import UpdateTeam from './UpdateTeam.js';
import '../manage_locations_page/Location.css'

function UpdateTeamPage(){
    const role = sessionStorage.getItem("role");
    let adminView = false;

    if (role === "1"){
        adminView = true
    } else if (role === "2"){
        adminView = false
    }

    return (
        <div id ="locationBG">
            {adminView && <UpdateTeam></UpdateTeam>}
            {!adminView && <CoachAddTeam></CoachAddTeam>}
        </div>
    )
}

export default UpdateTeamPage;