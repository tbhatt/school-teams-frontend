import React, { useEffect, useState } from 'react';
import { Button, Card, Container, Form, FormControl, FormLabel } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import * as URL from '../../shared/Const.js';

function AssignTeamPage(){
    const history = useHistory();

    const [number, setNumber] = useState();
    const [updateNumber, setUpdateNumber] = useState();
    const [playerUsername, setPlayerUsername] = useState("");
    const [updatePlayerUsername, setUpdatePlayerUsername] = useState("");

    const [team_id, setTeam_id] = useState();
    const [teamChosen, setTeamChosen] = useState(false);

    const [addUser, setAddUser] = useState(false);
    const [updateUser, setUpdateUser] = useState(false);

    const [deleteUser, setDeleteUser] = useState(false);

    const [allTeams, setAllTeams] = useState([]);
    const [allPlayers, setAllPlayers] = useState([]);
    const [allNotPlayers, setAllNotPlayers] = useState([]);

    const baseURL = URL.COACH_URL;
    const [coaches, setCoaches] = useState([]);
    const [username, setUsername] = useState();
    const [coachChosen, setCoachChosen] = useState(false);


    useEffect(() => {
        if (sessionStorage.getItem("role") === "1"){
            setCoachChosen(false);
        } else if (sessionStorage.getItem("role") === "2"){
            setUsername(sessionStorage.getItem("user"));
            setCoachChosen(true);
        }
        getAllCoaches();
        getAllTeams(sessionStorage.getItem("user"));
    }, [])


    function getAllCoaches(){
        fetch(baseURL + "user/coach", {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'} 
        }).then(function(response){
            if (response.status === 401) {
                alert("You don't have access to do this!")
                history.pushState("/home")
            }
            return response.json()
        }).then(function(data){
            setCoaches(data);
            return;
        })
    }

    function getAllTeams(user){
        fetch (baseURL + "team/coach/" + user, {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'} 
        }).then(function(response){
            return response.json();
        }).then(function(data){
            setAllTeams(data);
            setTeam_id(data[0])
            return;
        })
    }

    function getPlayersOnTeam(team){
        fetch (baseURL + "player/team/" + team, {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'} 
        }).then(function(response){
            return response.json();
        }).then(function(data){
            setAllPlayers(data);
            return;
        })
    }

    function getPlayersNotOnTeam(team){
        fetch (baseURL + "player/notPartOfTeam/" + team, {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'} 
        }).then(function(response){
            return response.json();
        }).then(function(data){
            setAllNotPlayers(data);
            return;
        })
    }

    const teamOptions = allTeams.map(team => {
        return (
            <option key = {team.id} value = {team.id}>{team.name}</option>
        )
    })

    const coachOptions = coaches.map(coach => {
        return (
            <option key = {coach.username} value = {coach.username}>{coach.username}</option>
        )
    })

    const newPlayerOptions = allNotPlayers.map(player => {
        return (
            <option key = {player.username} value = {player.username}>{player.username}</option>
        )
    })

    const updatePlayerOptions = allPlayers.map(player => {
        return (
            <option key = {player.username} value = {player.username}>{player.username}</option>
        )
    })

    function changeCoach(event){
        setUsername(event.target.value);
        setCoachChosen(true);
        getAllTeams(event.target.value);
    }

    function changeTeamId(event){
        setTeam_id(event.target.value);
        getPlayersOnTeam(event.target.value);
        getPlayersNotOnTeam(event.target.value);
        setTeamChosen(true);
    }

    function toggleAdd(){
        setAddUser(true);
        setUpdateUser(false);
    }

    function toggleUpdate(){
        setAddUser(false);
        setUpdateUser(true);
    }

    function changePlayerToAdd(event){
        setPlayerUsername(event.target.value)
    }

    function changeUpdatePlayer(event){
        setUpdatePlayerUsername(event.target.value);
    }

    function changePlayerNumber(event){
        setNumber(event.target.value);
    }

    function changeUpdateNumber(event){
        setUpdateNumber(event.target.value);
    }

    function addNewPlayer(){

        const playerRequest = {
            username: playerUsername,
            team_id: team_id,
            player_number: number
        };
        fetch(baseURL + "player/add", {
            method: 'POST',
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(playerRequest)
        }).then(function(response){
            return response.status;
        }).then(function(status){
            if (status === 201){
                alert("New player assigned to team!")
                setAddUser(false);
            } else {
                alert("Something went wrong! " + status);
            }
        })
    }

    function changeDelete(){
        if (deleteUser){
            setDeleteUser(false);
        } else {
            setDeleteUser(true);
        }
    }

    function updatePlayer(){
        if (deleteUser){
            let childUsername;
            if (updatePlayerUsername.length === 0){
                childUsername = allPlayers[0].username;
            } else {
                childUsername = updatePlayerUsername;
            }
            fetch(baseURL + "player/delete/" + childUsername, {
                method: 'DELETE',
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            }).then(function(response){
                return response.status
            }).then(function(status){
                if (status === 201){
                    alert("Players position in team deleted!")
                } else {
                    alert("Something went wrong!")
                }
            })
        } else {
            let childUsername;
            if (updatePlayerUsername.length === 0){
                childUsername = allPlayers[0].username;
            } else {
                childUsername = updatePlayerUsername;
            }
            const updateRequest = {
                player_number: updateNumber
            };

            fetch(baseURL + "player/update/team/" + team_id +"/player/" + childUsername, {
                method: 'PUT',
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
                body: JSON.stringify(updateRequest)
            }).then(function(response){
                return response.status;
            }).then(function(status){
                if (status === 201){
                    alert("Player updated!")
                    setAddUser(false);
                } else {
                    alert("Something went wrong! " + status);
                }
                return;
            })

        }
    }



    
    return (
        <div id ="locationBG">
            <Container>
                <h1 id ="white">Assign team players</h1>
                <Card >
                    {!coachChosen && <Form>
                <FormLabel>Coaches</FormLabel>
                <FormControl required as="select" onChange = {changeCoach}>
                    {coachOptions}
                </FormControl>                
            </Form>}

            {coachChosen && <Form>
                <FormLabel>Teams</FormLabel>
                <FormControl required as="select" onChange = {changeTeamId}>
                    {teamOptions}
                </FormControl>
            </Form>}
            <div>
            {teamChosen && <Button variant ="dark" onClick={toggleAdd}>Add new player to team</Button>}
            {teamChosen && <Button variant="dark" onClick={toggleUpdate}>Update existing player position</Button>}

            </div>


            {addUser && <Form>
                <FormLabel>Player to add</FormLabel>
                <FormControl required as ="select" onChange = {changePlayerToAdd}>
                    {newPlayerOptions}
                </FormControl>

                <FormLabel>Player number</FormLabel>
                <FormControl required type="number" onChange = {changePlayerNumber}></FormControl>

                <Button variant="dark" onClick = {addNewPlayer}>Add player</Button>
            </Form>}

            {updateUser && <Form>
                <FormLabel>Player to update</FormLabel>
                <FormControl required as ="select" onChange = {changeUpdatePlayer}>
                    {updatePlayerOptions}
                </FormControl>

                <FormLabel>Player number</FormLabel>
                <FormControl required type="number" onChange = {changeUpdateNumber}></FormControl>

                <FormLabel>Delete player?</FormLabel>
                <FormControl required type="checkbox" onChange = {changeDelete}></FormControl>

                <Button variant ="dark" onClick = {updatePlayer}>Update player</Button>    
            </Form>}

                </Card>

            </Container>
            

        </div>
    )
}

export default AssignTeamPage;