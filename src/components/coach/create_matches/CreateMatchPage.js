import Axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Button, Card } from 'react-bootstrap';
import LocationView from '../../admin_coach/manage_locations_page/LocationView';
import DateTimePicker from 'react-datetime-picker';
import * as URL from '../../shared/Const.js';
import { useHistory } from 'react-router-dom';
import LoadingOverlay from 'react-loading-overlay';

const CreateMatchPage = () => {
    
    const [username, setUsername] = useState(sessionStorage.getItem("user")); //!!!!!!Username for logged in COACH
    const SERVICE_URL = URL.COACH_URL;
    const AUTH_TOKEN = sessionStorage.getItem("token");
    Axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
    const header = {'Authorization' : AUTH_TOKEN,
                    'Content-type': 'application/json; charset=UTF-8'}
    
    //Data render
    const [teams, setTeams] = useState("");
    const [teamsOpp, setTeamsOpp] = useState(""); //Enemy teams 
    const [team, setTeam] = useState("");
    const [teamOpp, setTeamOpp] = useState("");
    const [oppReady, setOppReady] = useState(false);

    const [schools, setSchools] = useState("");
    const [school, setSchool] = useState("");

    //Step 3 - Locations
    const [locations, setLocations] = useState("");
    const [location, setLocation] = useState("");
    //Step 4 - Time
    const [date, setDate] = useState(new Date());
    const [dateFormat, setDateFormat] = useState("")

    //Changes 
    const [loading, setLoading] = useState(true);
    const [loading2, setLoading2] = useState(false);
    const [error, setError] = useState(false);
    const [step, setStep] = useState(1);
    const [valid, setValid] = useState(true);

    const history = useHistory();

    useEffect(() => {
        getTeamsByCoach();

    }, [])
    
    //Select enemy teams from chosen school & sport
    const getTeamsByCoach = async () => {
        console.log("Getting teams for coach " + username)
        const endpoint = `${SERVICE_URL}/team/coach/${username}`;
        await Axios.get(endpoint).then(res => {
            if (res.status === 401) {
                alert("You don't have access to this page!")
                history.pushState("/home")
            }
            console.log(res.data)
            setTeams(res.data);
            getLocations();
            if(res.data.length===0) setError(true);
        })
    }

    const getLocations = async () => {
        console.log("Getting locations")
        const endpoint = `${SERVICE_URL}/location`;
        console.log(endpoint)
        await Axios.get(endpoint)
            .then( response => {
                console.log(response.status)
                console.log(response.data)
                setLocations(response.data);
                setLocation(response.data[0]);
                setLoading(false);
            })
    }

    const getTeamsBySport = async () => {
        const endpoint = `${SERVICE_URL}/team/sport/${team.sport_id}`;
        await Axios.get(endpoint).then(res => {
            const filtered = res.data.filter(item => item.coach !== username);
            setTeamsOpp(filtered);
            if(filtered.length>1) setOppReady(true)
        })
    }

    const createMatch = async () => {
        setLoading2(true)
        const match = {
            "start_time": dateFormat,
            "location_id": location.id,
            "team_1_id": team.id,
            "team_2_id": teamOpp.id
        }
        const endpoint =`${SERVICE_URL}matches/create`
        console.log(match)
        await Axios.post(endpoint, match)
            .then(res => {
                if(res.status === 201){
                    setLoading2(false);
                    alert("Match created")
                } 
                else alert("Something went wrong")
            })
    }

    const locationClicked = (locationInfo) => { 
        console.log(locationInfo);
        setLocation(locationInfo);
    }
    
    const nextStep = async () => {
        setStep(step + 1);
        if(step===1) { //Gets opposing teams
            setValid(true)
            setLoading2(true)
            await getTeamsBySport();
            setLoading2(false)
        }

        if(step===4){ //Show a summary
            let year = date.getFullYear();
            let month = testFormat(''+(1+date.getMonth()));
            let day = testFormat(''+date.getDate());
            let hour = testFormat(''+date.getHours());
            let min = testFormat(''+date.getMinutes());

            const formatted = (`${year}-${month}-${day}T${hour}:${min}:00`)
            setDateFormat(formatted)
        }
        else if(step===5) { //Create match step
            createMatch();
        }
        else if(step===6){
            return "Match was created"
        } else if (step > 6){
            setStep(1);
        }
    }

    const cancel = (event) => {
        setStep(step-1);
    }

    //Function to make DateTime readable for database
    const testFormat = (v) => {
        if(v.length < 2) return ('0' + v);
        return v
    }

    const selectOppTeam = (event) => {
        const obj = JSON.parse(event.target.value)
        setTeamOpp(obj)
        console.log(obj)
        setValid(false)
    }
    const selectTeam = (event) => {
        const obj = JSON.parse(event.target.value)
        setTeam(obj)
        console.log(obj)
        setValid(false);
    }

    if (loading) return "loading..."

    return (
        <div id = "locationBG">
        <div className="container">
        <LoadingOverlay active={loading2} spinner text="loading">
            <div className="row"><h1 id ="white">Create match</h1></div>
            {step===1 && <div className="row">
                <h4 id ="white">Select home team</h4>
                <select onChange={selectTeam} class="custom-select mr-sm-2">
                    <option value="" selected disabled hidden>Choose here</option>
                    {teams.map(team => <option value={JSON.stringify(team)}>{team.name}</option>)}
                </select>
            </div>}

            
            {step===2 && <div className="row">
                    <h4 id ="white">Select away team</h4>
                    {oppReady && <select onChange={selectOppTeam} class="custom-select mr-sm-2">
                        <option value="" selected disabled hidden>Choose here</option>
                        {teamsOpp.map(team => <option value={JSON.stringify(team)}>{team.name}</option>)}
                    </select>}
            </div>}
            
            {step===3 && <div className="row">   
                <label id ="white" for="playground" class="col-sm-2 col-form-label">Playground:</label>
                <div class="col-sm-10">
                    <input type="text" id="playground" readonly class="form-control-plaintext" id = "white" value={location.name}/>
                </div>

                <Card id ="locations-list">
                    {locations.map(location => <LocationView key={location.id} location={location} locationClick={locationClicked}></LocationView>)}

                </Card>             
            </div>}

            {step===4 && <div className="row">
                <Card>
                    <DateTimePicker disableClock="true" 
                    value={date} onChange={setDate}></DateTimePicker>
                </Card></div>
            }

            {step===5 && <div>
                <div className="row"><h4 id ="white">Are you sure u want to create this match?</h4></div>
                <div className="row">
                    <label for="team1" class="col-sm-2 col-form-label">Team 1</label>
                    <div className="col">
                        <input type="text" id="team1" readonly class="form-control-plaintext" value={team.name}/>
                    </div>
                </div>
                <div className="row">
                    <label for="team2" class="col-sm-2 col-form-label">Team 2</label>
                    <div className="col">
                        <input type="text" id="team2" readonly class="form-control-plaintext" value={teamOpp.name}/>
                    </div>                    
                </div>
                <div className="row">
                    <label for="location" class="col-sm-2 col-form-label">Ground</label>
                    <div className="col">
                        <input type="text" id="location" readonly class="form-control-plaintext" value={location.name}/>
                    </div>                    
                </div>
                <div className="row">
                    <label for="date" class="col-sm-2 col-form-label">Time</label>
                    <div className="col">
                        <input type="text" id="date" readonly class="form-control-plaintext" value={dateFormat}/>
                    </div>                    
                </div>
            </div>}

            <div className="row" id ="bitmoreRight">
                {step>1 && step<6 && <Button variant="outline-dark"onClick={cancel} >Back</Button>} 
                {!error && <Button disabled={valid} variant="dark"onClick={nextStep}>Next</Button>}
            </div>    
            </LoadingOverlay>
        
        </div>
        </div>
    )
}

export default CreateMatchPage
