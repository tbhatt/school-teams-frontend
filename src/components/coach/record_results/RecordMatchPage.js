import React, { useEffect, useState } from 'react';
import { Card, Container, FormControl } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import * as URL from '../../shared/Const.js';
import ResultBox from './ResultBox.js';

function RecordMatchPage(){
    const baseURL = URL.COACH_URL;
    const [unrecordedMatches, setUnrecordedMatches] = useState([]);
    const [anyToRecord, setAnyToRecord] = useState(true);

    const [chosenMatch, setChosenMatch] = useState();
    const [selected, setSelected] = useState(true);
    const history = useHistory();

    useEffect(() => {
        fetch(baseURL + "matches/unregistered_result/coach/" + sessionStorage.getItem("user"), {
            headers: {'Authorization' : sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}})
            .then(function(response){
                if (response.status === 401) {
                    alert("You don't have access to this page!")
                    history.pushState("/home")
                }
                return response.json();
            }).then(function(data){
                setUnrecordedMatches(data);
                if (data.length === 0){
                    setAnyToRecord(false);
                }
                return;
            })
    }, [baseURL])

    function changeMatch(event){
        console.log(event.target.value);
        setChosenMatch(event.target.value);
        setSelected(true);
    }

    function execute(homeGoals, awayGoals){
        let result;
        if (homeGoals === awayGoals){
            result = "D"
        } else if (homeGoals > awayGoals){
            result = "H"
        } else {
            result = "A"
        }
        const intHGoals = parseInt(homeGoals);
        const intAGoals = parseInt(awayGoals);
        const intMatch = parseInt(chosenMatch);
        const POSTrequest = {
            home_team_goals: intHGoals,
            away_team_goals: intAGoals,
            winner: result,
            match_id: intMatch
        };

        console.log(POSTrequest)
        fetch(baseURL + "matches/create/match_result", {
            method: 'POST',
            headers: {'Authorization' : sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(POSTrequest)
        }).then(function(response){
            if (response.status === 201){
                alert("Results recorded!")
            } else {
                alert("Something went wrong!")
            }
        })
    }

    function toggleOff(){
        setSelected(false);
    }


    const matchOptions = unrecordedMatches.map(match => {

        return (
            <option key = {match.match_id} value = {match.match_id}>{match.home_team} vs {match.away_team}</option>
        )
    })

    return (
        <div id ="locationBG">
            <h1 id ="white">Record match results</h1>
            <Container>
            {anyToRecord && <Card>
                <FormControl as="select" onChange={changeMatch} onClick = {changeMatch}>
                    {matchOptions}
                </FormControl>
                {selected && <ResultBox execute = {execute} toggle = {toggleOff}></ResultBox>}
            </Card>}

            </Container>

            
            <Container>
                {!anyToRecord && <Card >
                    <h2>No unrecorded matches played!</h2>
                </Card>}
            </Container>
        </div>

    )
}


export default RecordMatchPage;