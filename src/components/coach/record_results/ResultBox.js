import React, { useState } from 'react';
import { Button, Card, FormControl, FormGroup, FormLabel } from 'react-bootstrap';

function ResultBox(props){
    const [home, setHome] = useState(0);
    const [away, setAway] = useState(0);

    function submitForm(){
        props.execute(home, away);
        
    }

    function setHomeGoals(event){
        setHome(event.target.value);
    }
    function setAwayGoals(event){
        setAway(event.target.value);
    }

    return (
        <div>
            <Card>
                <Card.Body>
                    <FormGroup>
                        <FormLabel>Home goals:</FormLabel>
                        <FormControl type="number" onChange = {setHomeGoals}></FormControl>
                        <FormLabel>Away goals:</FormLabel>
                        <FormControl type="number" onChange = {setAwayGoals}></FormControl>
                        <Button variant = "dark" onClick={submitForm}>Submit results</Button>
                    </FormGroup>
                </Card.Body>
            </Card>

        </div>
    )
}

export default ResultBox;