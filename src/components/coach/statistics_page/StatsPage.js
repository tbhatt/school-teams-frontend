import Axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom';
import StatsView from './StatsView';
import * as URL from '../../shared/Const';
import LoadingOverlay from 'react-loading-overlay';


const SchoolStats = (props) => {
    const [schoolStats, setSchoolStats] = useState("");
    const AUTH_TOKEN = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJKb3N0ZWluIiwiaWF0IjoxNjAzMTA2MTg3LCJzdWIiOiIxIiwiaXNzIjoiN2U0MDE0ZDE0ZTg0ZWEyYWIyZTFmNGEyZDYxYTA0M2Q5ZmE3ZTEyNGZiYTY5NGFlNjA1MDdmYWUwNTU0NzJkNCJ9.Y0b1ZdTPg7tEkBk1AI0FDk2c3dmZXJZuZxVxMbbhJiU';
    Axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

    //Admin rights
    const roleId = sessionStorage.getItem("role");
    let CURRENT_API;
    if(roleId==='1') CURRENT_API = URL.ADMIN_URL;
    else if(roleId==='2') CURRENT_API = URL.COACH_URL;
    else if(roleId==='3') CURRENT_API = URL.PARENT_URL;
    else if(roleId==='4') CURRENT_API = URL.PLAYER_URL;
    const username = sessionStorage.getItem("user"); 
    const [schools, setSchools] = useState([]);
    const [teams, setTeams] = useState([]);
    const [players, setPlayers] = useState([]);
    const history = useHistory();
    
    const [school, setSchool] = useState("");
    const [schoolStat, setSchoolStat] = useState("");
    const [team, setTeam] = useState("");
    const [teamStats, setTeamStats] = useState("");
    const [player, setPlayer] = useState("");
    const [playerStats, setPlayerStats] = useState("");


    const [lock, setLock] = useState(false);
    const [loading, setLoading] = useState(true);
    const [loading2, setLoading2] = useState(false);

    useEffect(() => {
        if(roleId === '1'){
            getSchools();
        }
        else if(roleId === '3') getChildren() //Parents go through their children
        else{ //Coach, player and parent can only view their own school
            getSchool();
        }

    }, [])

    const getSchool = async(relation) => { //For the others
        console.log("Getting school")
        let endpoint = `${CURRENT_API}school/user/${username}`;
        if(roleId==='4') endpoint = `${CURRENT_API}/school/player/${username}`;
        else if(roleId==='3') endpoint = `${CURRENT_API}/school/player/${relation.child}`;

        await Axios.get(endpoint).then(async res => {
            if (res.status === 401) {
                alert("You don't have access to this page!");
                history.pushState("/home");
            }
            else{
                console.log(res.data)
                setSchools([res.data]);
                setSchool(res.data);
                await getSchoolStat(res.data.id)
    
                if(roleId==='4') getTeamsForPlayer();
        
                else if(roleId==='1' || roleId==='2') getTeams(res.data.id);
            }
        }) 
    }   

    const getSchools = async () => { //For Admin
        console.log("Get schoools")
        const endpoint = `${CURRENT_API}/school`;
        await Axios.get(endpoint).then(async res => {
            if (res.status === 401) {
                alert("You don't have access to this page!")
                history.pushState("/home")
            }
            console.log(res.data)
            setSchools(res.data);
            setSchool(res.data[0])
            await getSchoolStat(res.data[0].id)
            await getTeams(res.data[0].id);
        })
    }

    const getTeams = async (schoolId) => { //Admin -- Coach
        console.log("Get teams")
        let endpoint = `${CURRENT_API}/team/school/${schoolId}`;
        await Axios.get(endpoint).then(async res => {
            setTeams(res.data);
            console.log(res.data)
            if(res.data.length > 0){
                setTeam(res.data[0])
                await getTeamStat(res.data[0].id)
                await getPlayers(res.data[0].id)
            }
            else{
                setLoading(false)
                alert("School has no teams")
            }
        })
    }

    const getTeamsForPlayer = async() => {
        console.log("Getting teams for player " + username);
        let endpoint = `${URL.PLAYER_URL}/team/player/${username}`;
        await Axios.get(endpoint).then(async res => {
            setTeams(res.data);
            console.log(res.data)
            if(res.data.length > 0){
                setTeam(res.data[0])
                //getPlayers(res.data[0].id)
                await getTeamStat(res.data[0].id)
                await getPlayerStat(username)
                const obj = {
                    username:username
                }
                setPlayers([obj])
                setPlayer(obj)
                setLoading(false)
            }
        })
    }

    const getTeamsForChildren = async(relation) => {
        console.log("Getting teams for child " + relation.child)
            console.log(relation.child)
            const endpoint = `${URL.PARENT_URL}/team/player/${relation.child}`
            try{
                await Axios.get(endpoint).then(async res => {
                    if(res.data.length > 0){
                        console.log(res.data);
                        setTeams(res.data)
                        setTeam(res.data[0])
                        await getTeamStat(res.data[0].id)
                        setLoading(false)
                    }
                    else alert("Your child has no teams")

                })
            }catch(error){
                console.log(error)
                alert("Fatal error getting your teams ")
            }
            
    }

    const getChildren = async () => {
        const endpoint = `${URL.PARENT_URL}/child/parent/${username}`
        try{
            console.log("Getting children")
            await Axios.get(endpoint).then(async res => {
                if(res.data.length > 0){
                    console.log(res.data)
                    setPlayers(res.data)
                    setPlayer(res.data[0])
                    await getSchool(res.data[0])
                    await getPlayerStat(res.data[0].child)
                    await getTeamsForChildren(res.data[0]);
                }
                else alert("You have no children");
            })

        } catch(error){
            console.log(error);
            alert("Fatal error getting children")
        }
    }

    const getPlayers = async (teamId) => {
        console.log("Getting players")
        if(roleId==='1') CURRENT_API = URL.COACH_URL; //Just because admin doesnt have
        const endpoint = `${CURRENT_API}/player/team/${teamId}`;
        await Axios.get(endpoint).then(res => {
            setPlayers(res.data);
            console.log(res.data)

            if(roleId==='1') CURRENT_API = URL.ADMIN_URL;
            setLoading(false)
        })
    }

    const getSchoolStat = async (schoolId) => {
        setLoading2(true)
        console.log('School stats')
        const endpoint = `${CURRENT_API}/statistic/stats/school/${schoolId}`;
        await Axios.get(endpoint).then(res1 => {
            console.log(res1.data)
            setSchoolStat(res1.data)
            setLoading2(false)
        })
    }

    const getTeamStat = async (teamId) => {
        console.log('Team stats')
        const endpoint = `${CURRENT_API}/statistic/stats/team/${teamId}`;
        await Axios.get(endpoint).then(res2 => {
            console.log(res2.data)
            setTeamStats(res2.data)
        })
    }

    const getPlayerStat = async (playerId) => {
        console.log('Player stats for ' + playerId)
        const endpoint = `${CURRENT_API}/statistic/stats/player/${playerId}`;
        await Axios.get(endpoint).then(res3 => {
            console.log(res3.data)
            setPlayerStats(res3.data)
        })
    }

    const onSelectSchool = async (event) => {
        if(roleId==='1'){
            setLoading2(true)
            const obj = JSON.parse(event.target.value);
            setLock(true);
            setSchool(obj)
            await getSchoolStat(obj.id)
            await getTeams(obj.id)
            setLock(false)
            setLoading2(false)
        }
    }

    const onSelectTeam = async(event) => {
        
        const obj = JSON.parse(event.target.value);
        setLoading2(true)
        setLock(true);
        setTeam(obj)
        if(roleId === '1' || roleId ==='2' ) await getPlayers(obj.id);

        await getTeamStat(obj.id)
        setLock(false);
        setLoading2(false)

    }

    const onSelectPlayer = async (event) => {
        const obj = JSON.parse(event.target.value);
        setLoading2(true)
        setPlayer(obj);
        //Get player stat
        console.log(obj)
        await getPlayerStat(obj.username)
        setLoading2(false)
    }

    const onSelectChild = async (event) => {
        const obj = JSON.parse(event.target.value);
        setLoading2(true)
        console.log(obj)
        await getTeamsForChildren(obj);
        await getPlayerStat(obj.child)
        setLoading2(false)
    }

    if(loading) {
        return "Loading..."
    }

    return (
        <div id = "locationBG">
        <div className="container">
            <LoadingOverlay active={loading2} spinner text="loading">
                <div className="row">
                    <div className="col"><h1 id ="white">Statistics page</h1></div>
                </div>
                <div className="row">
                    <div className="col">
                        <h4 id ="white">Schools</h4>
                        <select onChange={onSelectSchool} disabled={lock} class="custom-select mr-sm-2">
                            <option value="" selected disabled hidden>Change here</option>
                            {true&&schools.map(school => <option value={JSON.stringify(school)}>{school.name}</option>)}
                        </select>
                    </div>
                    <div className="col">
                        <h4 id ="white">Teams</h4>
                        <select onChange={onSelectTeam} disabled={lock} class="custom-select mr-sm-2">
                            <option value="" selected disabled hidden>Change here</option>
                            {teams.map(team => <option value={JSON.stringify(team)}>{team.name}</option>)}
                            {teams.length<1 && <option>No teams</option>}
                        </select>
                    </div>
                    {roleId!=='3' && <div className="col">
                        <h4 id ="white">Players</h4>
                        <select onChange={onSelectPlayer} disabled={lock} class="custom-select mr-sm-2">
                            <option value="" selected disabled hidden>Change here</option>
                            {players.length>0 && players.map(player => <option value={JSON.stringify(player)}>{player.username}</option>)}
                            {players.length<1 && <option>No players</option>}
                        </select>
                    </div>}

                    {roleId==='3' && <div className="col">
                        <h4 id ="white">Children</h4>
                        <select onChange={onSelectChild} disabled={lock} class="custom-select mr-sm-2">
                            <option value="" selected disabled hidden>Choose here</option>
                            {players.length>0 && players.map(player => <option value={JSON.stringify(player)}>{player.child}</option>)}
                            {players.length<1 && <option>No players</option>}
                        </select>
                    </div>}
                </div>

                <div className="row">
                    <div className="col">
                        <StatsView stats={schoolStat} obj={school}/>
                    </div>
                    <div className="col">
                        <StatsView stats={teamStats} obj={team}/>
                    </div>
                    <div className="col">
                        <StatsView stats={playerStats} obj={player}/>
                    </div>
                </div>
            </LoadingOverlay>
        </div>
        </div>
    )
}

export default SchoolStats
