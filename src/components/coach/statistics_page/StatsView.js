import React, { useEffect, useState } from 'react'

const StatsView = (props) => {
    const [obj,setObj] = useState("")
    const [stats, setStats] = useState("");

    useEffect(() => {
        setStats(props.stats);
        setObj(props.obj)
    }, [props])

    return (
        <div className="row">
            <div className="col">
                <div class="card">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            <h6>{obj.name}{obj.username}{obj.child} </h6>
                        </li>
                        <li class="list-group-item">
                            <p>Matches played: {stats.matches_played}</p>
                        </li>
                        <li class="list-group-item">
                            <p>Wins: {stats.wins} </p>
                            <p>Draws: {stats.draws}</p>
                            <p>Losses: {stats.losses} </p>

                        </li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default StatsView
