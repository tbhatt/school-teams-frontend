import React from 'react';
import { Dropdown, DropdownButton } from 'react-bootstrap';

function DropDownMenu(props){
    const options = props.optionMap;
    let dropdownOptions = options.map( (option) => 
        <Dropdown.Item key = {option.name} href = {option.href} >{option.name}</Dropdown.Item>
    );

    return (
        <div>
            <DropdownButton variant = "dark" id="dropdown-basic-button" title={props.title}>
                {dropdownOptions}
            </DropdownButton>
        </div>
        )
    }

export default DropDownMenu;