import React, { useState } from 'react';
import DropDownMenu from './DropDownMenu';
import './HomePage.css';
import LogoutButton from '../shared/logout_button/LogoutButton.js';
import { Image } from 'react-bootstrap';

function HomePage(){
    const [user, setUser] = useState(sessionStorage.getItem("user"));
    const [role, setRole] = useState(sessionStorage.getItem("role"));

    let adminUser = false;
    let coachUser = false;
    let parentUser = false;
    let childUser = false;

    if (role === '1'){adminUser = true; coachUser = false; parentUser = false; childUser = false;} 
    else if (role === '2'){adminUser = false; coachUser = true; parentUser = false; childUser = false}
    else if (role === '3'){adminUser = false; coachUser = false; parentUser = true; childUser = false}
    else if (role === '4'){adminUser = false; coachUser = false; parentUser = false; childUser = true}

    const parentOptions = [{href: '/profile/' + user, name: "Manage profiles"}, {href: '/message', name: "Message school"}, {href: "/settings", name:"Change settings"}];
    const childOptions = [{href: '/profile/' + user, name: "View profile"},  {href: '/message', name: 'Submit complaint'}, {href: "/settings", name:"Change settings"}];
    const adminOptions = [{href: '/create/users', name: "Manage users"}, {href: '/schools', name: "Manage schools"}, {href: '/locations', name: "Manage locations"}, {href: '/sports', name: "Manage sports"}, {href: '/teams', name: "Manage teams"}, {href: "/settings", name:"Change settings"}];
    const coachOptions = [{href: '/profile/' + user, name: "View profile"}, {href: '/create/users', name: "Manage users"}, {href: '/locations', name: "Manage locations"}, {href: '/sports', name: "Manage sports"}, {href: '/teams', name: "Manage teams"}, {href: "/settings", name:"Change settings"}];
    
    const uniqueAdmin = [{href: '/cancel', name: "Cancel matches"}, {href: '/notify', name: "Message parents"}, {href : '/corrections', name: "Manage corrections"}]
    const uniqueParents = [{href: '/matches', name: "Matches"}, {href: '/stats', name: "Game statistis"}];
    const uniqueChild = [{href: '/matches', name: "Matches"}, {href: '/stats', name: "Game statistis"}];
    const uniqueCoach = [{href: '/create/match', name: "Create match"}, {href: '/record/match', name: 'Record match results'}, {href: '/cancel', name: "Cancel matches"}, {href: '/assign/team', name: 'Assign team players'}, {href: '/stats', name: "Game statistis"}];

    return (
        <div id = "bgColor2">
            <div id = "top">
                <div id = "left">
                    {childUser && <DropDownMenu title = "View info" optionMap = {childOptions}></DropDownMenu>}
                    {parentUser && <DropDownMenu title = "Update info" optionMap = {parentOptions}></DropDownMenu>}
                    {coachUser && <DropDownMenu title = "Update info" optionMap = {coachOptions}></DropDownMenu>}
                    {adminUser && <DropDownMenu title = "Update info" optionMap = {adminOptions}></DropDownMenu>}
                </div>

                <div id = "right">
                    {adminUser && <DropDownMenu title = "Management" optionMap = {uniqueAdmin}></DropDownMenu>}
                    {parentUser && <DropDownMenu title = "Options" optionMap = {uniqueParents}></DropDownMenu>}
                    {childUser && <DropDownMenu title = "Options" optionMap = {uniqueChild}></DropDownMenu>}
                    {coachUser && <DropDownMenu title = "Options" optionMap = {uniqueCoach}></DropDownMenu>}

                </div>
            
            </div>
            <div id = "center">
                <div>
                    <h1 id = "white">Welcome, {user}!</h1>
                </div>

                <div>
                    <Image id = "image" src="https://cdn.pixabay.com/photo/2015/01/26/22/40/child-613199_1280.jpg" fluid />

                </div>
                
            </div>   

            <LogoutButton></LogoutButton>
        </div>
            

    )
}

export default HomePage;

