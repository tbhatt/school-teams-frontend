import React, { useEffect, useState } from 'react';
import QRCode from './QRCode.js';
import * as URL from '../shared/Const.js';
import { Card, Container, FormControl, FormLabel } from 'react-bootstrap';

function SettingsPage(){
    const [showQR, setShowQR] = useState(false);
    const [has2fa, setHas2fa] = useState(false);
    const [imageSRC, setImageSRC] = useState("");
    let baseUrl;

    if (sessionStorage.getItem("role") === "1"){
        baseUrl = URL.ADMIN_URL;
    } else if (sessionStorage.getItem("role") === "2"){
        baseUrl = URL.COACH_URL;
    }else if (sessionStorage.getItem("role") === "3"){
        baseUrl = URL.PARENT_URL;
    }else if (sessionStorage.getItem("role") === "4"){
        baseUrl = URL.PLAYER_URL;
    }

    useEffect(() => {
        fetch(baseUrl + "user/2fa/active/" + sessionStorage.getItem("user"), {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
        }).then(function(response){
            return response.json()
        }).then(function(data){
            setHas2fa(data.active);
        })
        
    }, [])

    function toggle2fa(new2fa){
        const postBody = {
            username: sessionStorage.getItem("user"),
            active: new2fa
        }
        console.log(postBody)
        fetch(baseUrl + "user/2fa", {
            method: "PATCH",
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(postBody)
        }).then(function(response){
            return response.status;
        }).then(function(status){
            if (status === 200){
                if (new2fa){
                    return fetch(baseUrl + "user/2fa/qr/" + sessionStorage.getItem("user"), {
                        headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
                    })
                } else {
                    setShowQR(false);
                    alert ("2 factor authorization turned off!")
                    return;
                }
            } else {
                alert("Sorry, something went wrong!")
                return;
            }
        }).then(function(response){
            if (response !== undefined){
                if (response.status === 400){
                    alert("You don't have a QR code registered! Please contact admins");
                    return;
                } else {
                    return response.json();
                }
            } else {
                return;
            }
        }).then(function(data){
            if (data !== undefined){
                console.log(data);
                setImageSRC(data.url);
                setShowQR(true);
            }
            return;
        })
    }

    function change2fa(event){
        const new2fa = !has2fa;
        setHas2fa(new2fa);
        toggle2fa(new2fa);
    }


    return (
        <div id ="locationBG">
            {showQR && <QRCode image={imageSRC}></QRCode>}

            <Container>
                
                <h1 id = "white">Settings</h1>
                <Card id = "halfMargin">
                    <FormLabel>Enable 2 factor authorization?</FormLabel>
                    <FormControl type="checkbox" checked ={has2fa} onChange = {change2fa}></FormControl>


                </Card>
            </Container>

        </div>
    )
}

export default SettingsPage;