import React from 'react';
import { Image } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import  './RegistrationDonePage.css';

function RegistrationDonePage(){
    let history = useHistory();

    function goToLogin(event){
        history.push('/login');
    }

    return (
        <div id = "bgColor">
            <div>
                <Image id = "bgImage" src="https://images.unsplash.com/photo-1547347298-4074fc3086f0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9" fluid />
                <div id = "overlay" onClick = {goToLogin}>
                    <h4>Password updated!</h4>
                </div>
            </div>
        </div>
    )
}

export default RegistrationDonePage;