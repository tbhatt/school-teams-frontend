import React from 'react';

import IntroBox from './IntroBox.js';
import './FrontPage.css';
import { Container } from 'react-bootstrap';


function FrontPage() {
    return (
        <div id = "bgColor">
            <Container fluid id = "bgColor">
                <IntroBox id = "centering"></IntroBox>
            </Container>
        </div>

    )
}

export default FrontPage;