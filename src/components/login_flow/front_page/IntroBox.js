import React from 'react';
import { useHistory } from "react-router-dom";

import './IntroBox.css';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';


function IntroBox(){
    let history = useHistory();

    function goToLogin(event){
        history.push('/login');
    };

    return (
        <div>
            <Card className = "shadow-lg  bg-white" id = "introBox">
                    <div id = "title">
                        <h2>School Teams</h2>
                    </div>
                    <div id = "button"> 
                        <Button variant="secondary" onClick = {goToLogin}>Login</Button>
                    </div>
            </Card>
        </div>
    );
}

export default IntroBox;