import React, { useState } from 'react';
import { Button, Card, Container, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import * as URL from '../../shared/Const.js';

function Auth(){
    let [validated, setValidated] = useState(false);

    const [code, setCode] = useState();
    const history = useHistory();
    function changeCode(event){
        setCode(event.target.value);
    }

    const doTwoAuth = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else {
            if (form.checkValidity() === true){
                
                event.preventDefault();
                const postBody = {
                    token: sessionStorage.getItem("token"),
                    code: code
                }
                fetch (URL.REGISTER_URL + "2fa", {
                    method: 'POST',
                    headers:  {'Content-type': 'application/json; charset=UTF-8'},
                    body: JSON.stringify(postBody)
                }).then(function(response){
                    if (response.status === 200){
                        return response.json();
                    } else if (response.status === 401){
                        alert("Wrong code!!")
                        return;
                    }
                }).then(function(data){
                    if (data){
                        sessionStorage.setItem("token", data.token)
                        history.push("/home")
                    }

                })
            }
        }
        setValidated(true);   
    }




    return (
        <div id = "bgColor">
            <Container fluid id = "bgColor">
                <Card id = "loginFormBox">
                    <Form noValidate validated={validated} onSubmit = {doTwoAuth}>
                        <FormGroup className="mb-3" >
                        <FormLabel>Code</FormLabel>
                        <FormControl onChange = {changeCode} minLength="6" maxLength="6" required size= "lg" id = "usernameInput" />
                        <FormControl.Feedback type = "invalid">Code too short</FormControl.Feedback>
                        <Button variant="secondary" type="submit" id = "button-color">Log in</Button>

                        </FormGroup>
                    </Form>
                </Card>
            </Container>
        </div>

    )
}


export default Auth;