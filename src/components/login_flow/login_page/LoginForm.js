import React, { useState } from 'react';
import { Button, Card, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

import './LoginForm.css';

import * as URL from '../../shared/Const.js';

function LoginForm(props){
    const history = useHistory();

    let [validated, setValidated] = useState(false);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const baseURL = URL.LOGIN_URL;

    function goToForgotPw(event){
        history.push('/forgotpw')
    }

    function changePassword(event){
        setPassword(event.target.value)
    }

    function changeUsername(event){
        setUsername(event.target.value)
    }

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        } else {
            if (form.checkValidity() === true){
                event.preventDefault();
                const postBody = {
                    username: username,
                    password: password
                }
                fetch (baseURL + "login", {
                    method: 'POST',
                    headers:  {'Content-type': 'application/json; charset=UTF-8'},
                    body: JSON.stringify(postBody)
                }).then(function(response){
                    if (response.status === 200){
                        return response.json();
                    } else if (response.status === 401){
                        alert("Wrong password!!")
                        return;
                    } else if (response.status === 404){
                        alert("User doesn't exist!")
                        return;
                    }
                }).then(function(data){
                    if (data){
                        sessionStorage.setItem("token", data.token)
                        sessionStorage.setItem("user", data.username)
                        sessionStorage.setItem("role", data.role_id)
                        if (data.auth){
                            history.push("/home");
                        } else {
                            history.push("/auth")
                        }
                    }

                })
        }
        setValidated(true);   
    }     

    };

    return (
            <Card className = "shadow-lg  bg-white" id = "loginFormBox">
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                <FormGroup className="mb-3" >
                    <FormLabel>Username</FormLabel>
                    <FormControl onChange = {changeUsername} required size= "lg" id = "usernameInput" />
                    <FormControl.Feedback type = "invalid">Please enter username</FormControl.Feedback>
                    
                    <FormLabel>Password</FormLabel>
                    <FormControl type = "password" onChange = {changePassword} required size= "lg" id = "passWordInput"/>
                    <FormControl.Feedback type = "invalid">Please enter password</FormControl.Feedback>
                    
                    <Button variant="secondary" type="submit" id = "button-color">LOGIN</Button>
                </FormGroup>
                </Form>
                <div id = "buttonContainer">
                    <Button variant="outline-secondary" type = "button" id = "bottomButtonRight" onClick = {goToForgotPw} >Forgot Pasword?</Button>
                </div>

        </Card>
    )
}

export default LoginForm;