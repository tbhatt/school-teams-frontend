import React from 'react';
import { Container } from 'react-bootstrap';
import LoginForm from './LoginForm.js';
import './LoginPage.css';

function LoginPage(props){

    return (
        <div id = "bgColor">
            <Container fluid id = "bgColor">
                <LoginForm setAll = {props.setAll}>
                </LoginForm>
            </Container>
        </div>

    )
}

export default LoginPage;