import React, { useState } from 'react'
import { Button, Card, Form } from 'react-bootstrap'
import * as URL from '../../shared/Const.js';
import './ResetPages.css';

const ForgotPassword = ({props}) => {
        //Setting up states and handlechange so we only need 1 handleChange 
        const [username, setUsername] = useState("");
        const [showMessage, setShowMessage] = useState(false);


    function handleChange(event){
        setUsername(event.target.value);
        console.log(event.target.name + " = " + event.target.value)
    };

    //Submit when register clicked
    function submitForm(){
        const apiURL = URL.EMAIL_URL + "recover";
        const request = {
            username: username
        }
        console.log("Reset clicked");
        console.log("Username: " + username);
        fetch(apiURL, {
            method: 'POST',
            headers: {'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(request)
        }).then(function(response){
            return response.status
        }).then(function(status){
            console.log(status)
            if (status === 200){
                setShowMessage(true);
            } else {
                alert("Something went wrong! Try again later")
            }
        })
    }

        return (
            <div id = "bgColor">
                <Card id = "form">
                    <Card.Body id = "margin">
                    <h1>Reset password</h1>
                    <Form>
                        <Form.Group controlId="formBasicUsername">
                            <Form.Label>Username:</Form.Label>
                            <Form.Control type="text" name="username" onChange={handleChange} placeholder="Enter username..." />
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>
                        <Button onClick={submitForm} variant="success">
                            Reset password
                        </Button>
                        {showMessage && <p>A reset link will shortly be sent to the associated email if username is correct</p>}     
                    </Form>
                    </Card.Body>                
                </Card>
            </div>
        )
}
export default ForgotPassword;