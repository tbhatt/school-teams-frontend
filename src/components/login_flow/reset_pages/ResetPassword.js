import React, { useState } from 'react'
import { Button, Card, Form } from 'react-bootstrap'
import { useHistory, useParams } from 'react-router-dom';
import * as URL from '../../shared/Const.js';
import './ResetPages.css';


const ResetPassword = ({props}) => {
    const params = useParams();
    const history = useHistory();
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");
    
    //Handle change for all inputs
    function handleChange(event){
        if (event.target.name === "password1"){
            setPassword1(event.target.value);
        } else {
            setPassword2(event.target.value);
        }
        console.log(event.target.name + " = " + event.target.value);
    };

    //Submit when register clicked
    function submitForm(){
        console.log("Reset clicked");
        console.log("Password 1: " + password1);
        console.log("Password 2: " + password2);

        if(password1 === password2){
            console.log("Passwords match");
            const apiURL = URL.EMAIL_URL + "recover/change";
            const request = {
                password: password1,
                token: params.token
            }
            fetch(apiURL, {
                method: 'POST',
                headers: {'Content-type': 'application/json; charset=UTF-8'},
                body: JSON.stringify(request)
            }).then(function(response){
                return response.status
            }).then(function(status){
                console.log(status)
                if (status === 200){
                    history.push('/complete')
                } else {
                    alert("Something went wrong! Try again later")
                }
            })

        } else {
            alert("Passwords don't match!")
        }
    }

        return (
            <div id = "bgColor">
                <Card id = "form2">
                    <Card.Body id = "margin"> 
                    <h1>Reset password</h1>
                    <Form>
                        <Form.Group controlId="formBasicPassword1">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password1" onChange={handleChange} placeholder="Enter password..." />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword2">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" name="password2" onChange={handleChange} placeholder="Enter the same password..." />
                        </Form.Group>

                        <Button onClick={submitForm} variant="success">
                            Reset password
                        </Button>
                    </Form>
                    </Card.Body>
                </Card>
            </div>
        )   
}

export default ResetPassword