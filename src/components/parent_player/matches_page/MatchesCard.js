import React, { useState } from 'react';
import { Button, Card, ListGroup } from 'react-bootstrap';


function MatchesCard(props){
    const matches = props.matches;
    const [toggleExtend, setToggleExtend] = useState(false);
    const [extendedMatch, setExtendedMatch] = useState("");

    function getExtendedInfo(getIndex){
        const extMatch = matches[getIndex];
        setExtendedMatch(extMatch);
        setToggleExtend(true);
    }

    function toggleOff(){
        setToggleExtend(false);
    }

    let index = -1;
    const cardMatches = matches.map(match => {
        index ++;
        match.index = index;
        return (
            <ListGroup.Item key = {match.index} action onClick = {() => getExtendedInfo(match.index)}>{match.field_name} {match.start_time}</ListGroup.Item>
        )
    })


    return (
        <div>
            <h4 id ="white">{props.title}</h4>
            <Card id ="matches-list">
                <ListGroup variant="flush">
                    {cardMatches}
                </ListGroup>
            </Card>
            {toggleExtend &&
            <Card id ="overlay2">
                <Card.Title>Field: {extendedMatch.field_name}</Card.Title>
                <Card.Body>
                    <Button variant ="dark" onClick={toggleOff}>X</Button>
                    <ListGroup>
                        <ListGroup.Item>
                            Away: {extendedMatch.away_team} vs Home: {extendedMatch.home_team}
                        </ListGroup.Item>
                        <ListGroup.Item>
                            Start time: {extendedMatch.start_time}
                        </ListGroup.Item>
                        <ListGroup.Item>
                            Results: {extendedMatch.away_team_goals} - {extendedMatch.home_team_goals}
                        </ListGroup.Item>
                        <ListGroup.Item>
                            Address: {extendedMatch.street_name} {extendedMatch.street_number}
                        </ListGroup.Item>
                    </ListGroup>
                </Card.Body>
            </Card>
            }
        </div>
    )
}

export default MatchesCard;