import React, { useEffect, useState } from 'react';
import { Button, Card, Container, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import MatchesCard from './MatchesCard.js';
import * as URL from '../../shared/Const.js';
import { useHistory } from 'react-router-dom';


function MatchesPage(){
    const parentURL = URL.PARENT_URL;
    const playerURL = URL.PLAYER_URL;

    //change between parent and player view
    const [parentView, setParentView] = useState(false);
    const [parent, setParent] = useState(sessionStorage.getItem("user"));
    const [child, setChild] = useState(sessionStorage.getItem("user"));


    const [allChildren, setAllChildren] = useState([]);

    const [title, setTitle] = useState("All matches");

    const [currentView, setCurrentView] = useState([]);
    const [childFilter, setChildFilter] = useState(false);

    const [allMatches, setAllMatches] = useState([]);
    const [upcomingMatches, setUpcomingMatches] = useState([]);
    const [previousMatches, setPreviousMatches] = useState([]);

    const [childMatches, setChildMatches] = useState([]);
    const [upcomingChildMatches, setUpcomingChildMatches] = useState([]);
    const [previousChildMatches, setPreviousChildMatches] = useState([]);
    const history = useHistory();

    useEffect(() => {
        if (sessionStorage.getItem("role") === "3"){
            setParentView(true);
        } else if (sessionStorage.getItem("role") === "4"){
            setParentView(false);
        }
        if (sessionStorage.getItem("role") === "3"){
            fetch (parentURL + "child/parent/" + parent, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            })
            .then(function(response){
                if (response.status === 401) {
                    alert("You don't have access to this page!")
                    history.pushState("/home")
                }
                return response.json()
            }).then(function(data){
                setAllChildren(data);
                return fetch(parentURL + "matches", {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                })
            }).then(function(response){
                return response.json()
            }).then(function(data){
                setAllMatches(data)
                setCurrentView(data);
                return fetch(parentURL + "matches/upcoming", {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                })
            }).then(function(response){
                return response.json()
            }).then(function(data){
                setUpcomingMatches(data)
                return fetch(parentURL + "matches/previous", {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                })
            }).then(function(response){
                return response.json()
            }).then(function(data){
                setPreviousMatches(data);
                return;
            })
        } else {
            const childValue = {
                child: child
            };
            setAllChildren([childValue]);
            fetch(playerURL + "matches", {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            }).then(function(response){
                if (response.status === 401) {
                    alert("You don't have access to this page!")
                    history.pushState("/home")
                }
                return response.json()
            }).then(function(data){
                setAllMatches(data)
                setCurrentView(data);
                return fetch(playerURL + "matches/upcoming", {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                })
            }).then(function(response){
                return response.json()
            }).then(function(data){
                setUpcomingMatches(data)
                return fetch(playerURL + "matches/previous", {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                })
            }).then(function(response){
                return response.json()
            }).then(function(data){
                setPreviousMatches(data);
                return;
            })
        }
    }, [parentView, child])

    function changeView (event){
        if (childFilter){
            if (event.target.value === "previous"){
                setTitle("Previous child matches")
                setCurrentView(previousChildMatches)
            } else if (event.target.value === "all"){
                setTitle("All child matches")
                setCurrentView(childMatches)
            } else if (event.target.value === "upcoming"){
                setTitle("Upcoming child matches")
                setCurrentView(upcomingChildMatches)
            }
        } else {
            if (event.target.value === "previous"){
                setTitle("Previous matches")
                setCurrentView(previousMatches)
            } else if (event.target.value === "all"){
                setTitle("All matches")
                setCurrentView(allMatches)
            } else if (event.target.value === "upcoming"){
                setTitle("Upcoming matches")
                setCurrentView(upcomingMatches)
            }
        }
    }

    function changeChild(event){
        let value = event.target.value;
        if (value === "0"){
            setChildFilter(false);
            if (title === "Previous child matches"){
                setTitle("Previous matches")
                setCurrentView(previousMatches)
            } else if (title === "All child matches"){
                setTitle("All matches")
                setCurrentView(allMatches)
            } else if (title === "Upcoming child matches"){
                setTitle("Upcoming matches")
                setCurrentView(upcomingMatches)
            }
        } else {
            let baseURL;
            if (parentView){
                baseURL = parentURL;
            } else {
                baseURL = playerURL;
            }

            setChildFilter(true);
            fetch(baseURL + "matches/previous/" + value, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            })
            .then(function(response){
                return response.json()
            }).then(function(data){
                if (title === "Previous matches" || title === "Previous child matches"){
                    setTitle("Previous child matches")
                    setCurrentView(data);
                }
                setPreviousChildMatches(data);
                return fetch(baseURL + "matches/upcoming/" + value, {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                })
            }).then(function(response){
                return response.json()
            }).then(function(data){
                if (title === "Upcoming matches" || title === "Upcoming child matches"){
                    setTitle("Upcoming child matches")
                    setCurrentView(data);
                }
                setUpcomingChildMatches(data)
                return fetch(baseURL + "matches/" + value, {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                })
            }).then(function(response){
                return response.json()
            }).then(function(data){
                if (title === "All matches" || title === "All child matches"){
                    setTitle("All child matches")
                    setCurrentView(data);
                }
                setChildMatches(data);
            })
        }
    }

    const childOptions = allChildren.map(child => {
        if (parentView){
            return (
                <option key = {child.child} value = {child.child}>{child.child}</option>
            )
        } else {
            return (
                <option key = {child.child} value = {child.child}>{child.child}</option>
            )
        }

    })
    
    return(
        <div id ="locationBG">
            <h1 id ="white">Matches</h1>
            <Container>
            <div>
                <Card id ="width">
                    <div>
                    <Button variant = "outline-dark" value = {"previous"} onClick = {changeView}>Previous</Button>
                <Button variant = "dark" value = {"all"} onClick = {changeView}>All</Button>
                <Button variant = "outline-dark" value = {"upcoming"} onClick = {changeView}>Upcoming</Button>

                    </div>
                <Form>
                    <FormGroup>
                        <FormLabel>Filter options</FormLabel>
                        <FormControl required as="select" onChange = {changeChild}>
                            <option key = "all" value = "0">All matches</option>
                            {childOptions}
                        </FormControl>
                    </FormGroup>
                </Form>

                </Card>

            </div>
            <div id ="iWantThisCentered">
                <MatchesCard title = {title} matches = {currentView}></MatchesCard>

            </div>
            </Container>
        </div>
    )
}

export default MatchesPage;