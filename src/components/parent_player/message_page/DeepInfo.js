import React from 'react';
import { Button, Card } from 'react-bootstrap';

function DeepInfo(props){
    const message = props.message;

    return (
        <div>
            <Card id  ="fit">
                <Card.Body>
                    <Card.Title>To: {message.receiver}</Card.Title>
                    <Card.Subtitle>From: {message.sender}</Card.Subtitle>
                    <Card.Text>{message.message}</Card.Text>
                    <Button variant="dark" onClick = {props.exit}>X</Button>
                </Card.Body>
            </Card>
        </div>
    )
}

export default DeepInfo;