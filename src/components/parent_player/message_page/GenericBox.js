import React, { useState } from 'react';
import { Card, ListGroup } from 'react-bootstrap';
import DeepInfo from './DeepInfo.js';

function GenericBox(props){
    const [messageShowing, setMessageShowing] = useState("");
    const ogMessages = props.messages;
    const [showMessage, setShowMessage] = useState(false);

    function toggleShowMessage(event){
        const messageToShow = ogMessages[event.target.value];
        setMessageShowing(messageToShow);
        setShowMessage(true);
    }

    let index = -1;
    const messages = props.messages.map(message => {
        index ++;
        const actualMessage = message.message;
        const smallMessage = actualMessage.slice(0, 25);
        return (
            <ListGroup.Item key = {message.message_id} value = {index} action onClick={toggleShowMessage}>
                {smallMessage}
            </ListGroup.Item>
        )
    });

    function toggleOff(){
        setShowMessage(false);
    }

    return (
        <div>
            {!showMessage && <Card id = "message-list">
                
                {messages}

            </Card>}

            {showMessage && <DeepInfo message={messageShowing} exit={toggleOff}></DeepInfo>}
        </div>
    )
}

export default GenericBox;