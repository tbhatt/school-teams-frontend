import React from 'react';
import { Card } from 'react-bootstrap';
import GenericBox from './GenericBox.js';


function InboxComponent(props){

    return (
        <div>
                <GenericBox messages = {props.inbox}></GenericBox>
        </div>
    )
}

export default InboxComponent;