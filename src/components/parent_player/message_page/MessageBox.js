import React, { useState } from 'react';
import { Button, Card, Form, FormControl, FormGroup, FormLabel } from 'react-bootstrap';
import GenericBox from './GenericBox.js';
import * as URL from '../../shared/Const.js';

function MessageBox(props){
    let baseURL;
    if (sessionStorage.getItem("role") === "3"){
        baseURL = URL.PARENT_URL + "message/send"
    } else if (sessionStorage.getItem("role") === "4"){
        baseURL = URL.PLAYER_URL + "message/send"
    }
    const [admin, setAdmin] = useState("");
    const [message, setMessage] = useState("");

    function submitMessage(event){
        event.preventDefault();
        const POSTrequest = {
            sender: props.user,
            receiver: admin,
            message: message
        };

        fetch((baseURL), {
            method: 'POST',
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'},
            body: JSON.stringify(POSTrequest)
        }).then(function(res){
            console.log(res.status)
            return res.status
        }).then(function(status){
            if (status === 201){
                alert("Message sent!")
            } else {
                alert("Something went wrong!")
            }
            return;
        })
    }

    function changeMessage(event){
        setMessage(event.target.value);
    }

    function changeAdmin(event){
        setAdmin(event.target.value);
    }

    const adminOptions = props.admins.map(admin => {
        return (
            <option key = {admin.username} value = {admin.username}>{admin.username}</option>
        )
    })

    return (
        <div>
            <Card id ="messages-list">
                <Card id = "half">
                <Card.Title>Send messages</Card.Title>
                    <Form onSubmit = {submitMessage}>
                    <FormGroup>
                        <FormLabel>Message</FormLabel>
                            <FormControl as="textarea" required size= "sm" id = "teamname" placeholder="The message you want to send" onChange={changeMessage} />        
                            <FormLabel>Admin</FormLabel>
                            <FormControl required as="select" onChange = {changeAdmin}>
                                {adminOptions}
                            </FormControl>
                        </FormGroup>
                    <Button variant="secondary" type="submit" id = "button-color">Send message</Button>
                    </Form>

                </Card>
                <div>
                <Card.Title>Previously sent</Card.Title>
                    <GenericBox messages = {props.sent}></GenericBox>

                </div>





            </Card>
        </div>
    )
}

export default MessageBox;