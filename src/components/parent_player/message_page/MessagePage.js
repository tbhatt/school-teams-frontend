import React, { useEffect, useState } from 'react';

import InboxComponent from './InboxComponent.js';
import MessageBox from './MessageBox.js';

import * as URL from '../../shared/Const.js';
import { Container } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

function MessagePage(){
    let baseURL;
    if (sessionStorage.getItem("role") === "3"){
        baseURL = URL.PARENT_URL;
    } else if (sessionStorage.getItem("role") === "4"){
        baseURL = URL.PLAYER_URL;
    }

    const [username, setUsername] = useState(sessionStorage.getItem("user"));
    const history = useHistory();
    const [admins, setAdmins] = useState([]);
    const [sentMessages, setSentMessages] = useState([]);
    const [inbox, setInbox] = useState([]);


    useEffect(() => {

        fetch(baseURL + "user/admin", {
            headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
        })
            .then(function(response){
                if (response.status === 401) {
                    alert("You don't have access to do this!")
                    history.pushState("/home")
                }
                return response.json()
            }).then(function(data){
                setAdmins(data)
                return fetch((baseURL + "message/sent/" + username), {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                });
            }).then(function(response){
                return response.json()
            }).then(function(data){
                setSentMessages(data);
                return fetch((baseURL + "message/inbox/" + username), {
                    headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
                })
            }).then(function(response){
                return response.json()
            }).then(function(data){
                setInbox(data);
                return;
            })
    }, [username, baseURL])

    return(
        <div id ="locationBG">
            <h2 id ="white">Messages</h2>
            <div id = "marginCenter">
                <h4 id ="white">Inbox</h4>
                <InboxComponent user = {username} inbox = {inbox}></InboxComponent>

            </div>

            <div></div>
            <div></div>
            <Container>
            <MessageBox user = {username} admins = {admins} sent = {sentMessages}></MessageBox>

            </Container>
        </div>
    )
}

export default MessagePage;