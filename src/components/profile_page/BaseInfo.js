import React, { useEffect, useState } from 'react'
import { Button } from 'react-bootstrap';

const BaseInfo = (props) => {
    //Base stats
    const [fname, setFname] = useState("Unknown"); 
    const [lname, setLname] = useState("Unknown");
    const [email, setEmail] = useState("Unknown");
    const [gender, setGender] = useState("Unknown");
    const [male, setMale] = useState(false);
    const [noGender, setNoGender] = useState(false);

    const [readOnly, setReadOnly] = useState(true);
    const [edit, setEdit] = useState(false);
    const [showEdit, setShowEdit] = useState(false);
    useEffect(() => {
        setFname(props.baseInfo.firstname); 
        setLname(props.baseInfo.lastname);
        setEmail(props.baseInfo.email);
        setGender(props.baseInfo.gender);
        if( (props.parent===false && props.player===false) || 
            (props.parent===true && props.player===true)){
            setShowEdit(true)
        }
        const genderr = props.baseInfo.gender;
        if(genderr === null) setNoGender(true)
        else if(genderr.toLowerCase() === 'male') setMale(true);
        else if(genderr.toLowerCase() === 'female') setMale(false);
        
    }, [props.baseInfo])

    const submitChanges = () => {
        if(gender==="male" || gender==="Male" || gender==="female" || gender==="Female"){
            const info = {
                "firstname": fname.trim(),
                "lastname": lname.trim(),
                "email": email,
                "gender": gender
            }
            props.updateBase(info)
            setEdit(!edit);
            setReadOnly(!readOnly)        }
        else{
            alert("Gender can only be Male or Female")
        }

    }
    const onSelectGender = (event) => {
        setGender(event.target.value);
    }
    const editClick = () => {
        setEdit(!edit);
        setReadOnly(!readOnly)
    }

    //Change to default values and remove onChange when features are decided
    return (
        <div className="container">
            <div className="row">
                <div className="col">Firstname
                    <input className="form-control" type="text" readOnly={readOnly}
                    onChange={e => setFname(e.target.value)}
                    value={fname}
                    />
                </div>
                <div className="col">Lastname
                    <input className="form-control" type="text" readOnly={readOnly}
                    onChange={e => setLname(e.target.value)}
                    value={lname}/>
                </div>
            </div>

            <div className="row">
                <div className="col">Email
                    <input className="form-control" type="email" readOnly={readOnly}
                    onChange={e => setEmail(e.target.value.trim())}
                    value={email}/>
                </div>
                <div className="col">Gender
                    <select onChange={onSelectGender} disabled={readOnly} class="custom-select mr-sm-2">
                        {noGender && <option selected={noGender}>No gender chosen</option>}
                        <option selected={male && !noGender}value="Male">Male</option>
                        <option selected={!male && !noGender} value="Female">Female</option>
                    </select>                    
                </div>
            </div><br/>
            {showEdit && <Button variant="dark" onClick={editClick}>Edit</Button>}
            {edit && <Button variant="dark" onClick={submitChanges}>Save changes</Button>}    
        </div>
    )
}
export default BaseInfo
