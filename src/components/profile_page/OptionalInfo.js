import React, { useState } from 'react'
import { useEffect } from 'react';
import { Button } from 'react-bootstrap';

const OptionalInfo = (props) => {
    //Show form check -- Only Players can "check"
    const [hideChecks, setHideChecks] = useState(true); //Only false for player
    const [hideButtons, setHideButtons] = useState(true); //Only false for parent
    const [editable, setEditable] = useState(true);
    const [disableBTN, setDisableBTN] = useState(true);

    //Optional information
    const [DOB, setDOB] = useState("");
    const [phone, setPhone] = useState("");
    const [medical, setMedical] = useState("");
    const [PP, setPP] = useState("");

    //Hide optional information
    const [hideDOB, setHideDOB] = useState(false); //Date of birth
    const [hidePhone, setHidePhone] = useState(false); //Phone
    const [hideMedical, setHideMedical] = useState(false); //Medical
    const [hidePP, setHidePP] = useState(false); //Profile picture

    useEffect(() => {
        //3 fields + PP
        if(props.optionalInfo.date_of_birth ==  null) setDOB(props.optionalInfo.date_of_birth);
        else setDOB((props.optionalInfo.date_of_birth.substring( 0,10)));
        
        setPhone(props.optionalInfo.mobile_number);
        setMedical(props.optionalInfo.medical_notes);
        setPP(props.optionalInfo.profile_picture)
    
        //For the value in check inputs
        setHideDOB((props.optionalInfo.date_of_birth_shared));
        setHidePhone(props.optionalInfo.mobile_number_shared);
        setHideMedical(props.optionalInfo.medical_notes_shared);
        setHidePP(props.optionalInfo.profile_picture_shared);
        
        //For coach none of these show.
        setHideChecks(!props.player) //Show checks if player
        setHideButtons(!props.parent) //Show buttons if parent

    }, [props.optionalInfo])

    const submitChecks = () => {
        const shared = {
            "date_of_birth_shared": hideDOB,
            "mobile_number_shared": hidePhone,
            "medical_notes_shared": hideMedical,
            "profile_picture_shared": hidePP
        }
        props.updateHidden(shared);
    }

    const submitChanges = () => {
        const info = {
            "date_of_birth": DOB+'T09:00:00',
            "mobile_number": phone,
            "profile_picture":PP,
            "medical_notes": medical
        }
        props.updateOptional(info);
        setEditable(!editable)
        setDisableBTN(!disableBTN)
    }

    const editClick = () => {
        setEditable(!editable)
        setDisableBTN(!disableBTN)
    }

    return (
        <div className="container">
            <div className="row">
                <div className="col">Date of birth</div>
                <div className="col">Phone</div>
            </div>

            <div className="row">
                <div className="col">
                    <input className="form-control" type="date" id="example-date-input"
                    onChange={e => setDOB(e.target.value)}
                    value = {DOB}
                    readOnly={editable}
                    />
                </div>

                <div className="col-md-1">
                    {!hideChecks && <label className="form-check-label">
                    <input className="form-check-input" type="checkbox"
                    onChange={e => setHideDOB(e.target.checked)}
                    checked={hideDOB}/> Visible
                    </label>}
                </div>

                <div className="col">
                    <input className="form-control" type="tel" id=""
                    onChange={e => setPhone(e.target.value.trim())}
                    value={phone}
                    readOnly={editable}/>
                </div>

                <div className="col-md-1">
                    {!hideChecks && <label className="form-check-label">
                    <input className="form-check-input" type="checkbox"
                    onChange={e => setHidePhone(e.target.checked)}
                    checked={hidePhone}/> Visible
                    </label>}
                </div>
            </div>

            <div className="row">
                <div className="col">
                    <label for="exampleTextarea">Medical Notes</label>
                    <textarea className="form-control" id="exampleTextarea" rows="3"
                    onChange={e => setMedical(e.target.value)}
                    value={medical}
                    readOnly={editable}></textarea>
                </div>
            </div>
            <div className="row">
                <div id = "littleMoreRight" className="col">
                    {!hideChecks && <label className="form-check-label">
                    <input className="form-check-input" type="checkbox"
                    onChange={e => setHideMedical(e.target.checked)}
                    checked={hideMedical}/>Visible</label>}
                </div>
                {false &&<div className="col">
                    {!hideChecks && <label className="form-check-label">
                    <input className="form-check-input" type="checkbox"
                     onChange={e => setHidePP(e.target.checked)}
                     checked={hidePP}/> PP Visible</label>}
                </div>}
            </div>
            <br></br>
            {!hideButtons && <Button variant="dark" onClick={editClick}>Edit</Button>}
            {!hideButtons && <Button variant="dark" onClick={submitChanges} disabled={disableBTN}>Save changes</Button>}    
            {!hideChecks && <Button variant="dark" onClick={submitChecks}>Update visibility</Button>}   
        </div>
    )
}
export default OptionalInfo
