import Axios from 'axios';
import React, { useEffect, useState } from 'react'
import LoadingOverlay from 'react-loading-overlay';
import {useHistory, useParams } from 'react-router-dom';
import BaseInfo from './BaseInfo';
import OptionalInfo from './OptionalInfo';
import * as URL from './../shared/Const.js';
import { Card } from 'react-bootstrap';

const ProfilePage = ({match}) => {
    const params = useParams();
    const history = useHistory()
    //Only one of these will be used depending on who is logged in.
    const PLAYER_URL = URL.PLAYER_URL;
    const PARENTS_URL = URL.PARENT_URL;

    //Roles -- Both False for Coach. Nothing clickable -- SELECT SERVICE & ROLE until log in fixed
    let CURRENT_API //Which service logged in user uses
    const [player, setPlayer] = useState(false); //Player can only READ and Check 'hide'
    const [parent, setParent] = useState(false); //Parents can EDIT optional info
    const [profileRole, setProfileRole] = useState(0);
    const [roleId] = useState(sessionStorage.getItem("role")); 

    //Change how PERSONID/USERNAME params work further in time when COACH is done
    const username = params.username; //params.username
    const [loading, setLoading] = useState(true);
    const [loading2, setLoading2] = useState(false);
    const [access, setAccess] = useState(true);
    //Data
    const [baseInfo, setBaseInfo] = useState("");
    const [optionalInfo, setOptionalInfo] = useState("");
    const [relations, setRelations] = useState([])
    const [picture, setPicture] = useState("")
    useEffect(()=> {
        if(roleId === "1"){
            setParent(true);
            setPlayer(true);  
            getInfo()  
        }

        else if(roleId === "2"){
            CURRENT_API = URL.COACH_URL;
            if(sessionStorage.getItem("user")===username) { //Coach visits own profile
                //setParent(true); Add if you want coach to edit profile
                getBaseInfo();
            } else{ //Coach visits player
                getInfo()
            }
        } 
        else if (roleId === "3"){
            setParent(true);
            CURRENT_API = URL.PARENT_URL;

            if(sessionStorage.getItem("user")===username) { //Parent visits own profile
                setProfileRole(3)
                getChildren()
                getBaseInfo()
            }
            else { //Parent visits child
                setProfileRole(4) 
                getBaseInfo();
            }
        } 
        else if (roleId === "4"){
            setPlayer(true);
            setProfileRole(4) 
            CURRENT_API = URL.PLAYER_URL;
            getBaseInfo()
        } 
    }, [])

    const getInfo = async () => {
        try{
            await Axios.get(`${URL.COACH_URL}/role/username/${username}`, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            })
                .then( response => {
                    setProfileRole(response.data.id)
                
                    if(response.data.id === 4 ){
                        getParents()
                        getBaseInfo()
                    }
                    else{
                        if(response.data.id === 3) {
                            getChildren()
                        }
                        getBaseInfo();
                    }
                })
        } catch(error) {
            alert("Error loading site: " + error)
        }
    }

    const getBaseInfo = async () => {
        try{
            if(roleId==='1') CURRENT_API = URL.PARENT_URL;
            await Axios.get(`${CURRENT_API}person/username/${username}`, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            }).then(response => {
                setBaseInfo(response.data)
                setPicture(`https://avatars.dicebear.com/api/${response.data.gender.toLowerCase()}/${username}.svg`)
                getOptionalInfo()
            })
        } catch(error) {
            alert("Error loading base info: " + error)
        }
    }

    const getOptionalInfo = async () => {
        try{
            if(roleId==='1') CURRENT_API = URL.PARENT_URL;
            await Axios.get(`${CURRENT_API}/optional/user/${username}`, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            })
                .then( response2 => {
                    setOptionalInfo(response2.data)
                    if(roleId==='1') CURRENT_API = URL.ADMIN_URL;
                    setLoading(false)
            })
        } catch(error) {
            alert("Error loading optional info: " + error)
        }
    }

    const getChildren = async () => {
        try{
            if(roleId==='1') CURRENT_API = PARENTS_URL;
            await Axios.get(`${CURRENT_API}/child/parent/${username}`, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            })
                .then(response => {
                    setRelations(response.data)
                    if(roleId==='1') CURRENT_API = URL.ADMIN_URL;
                })
        } catch(error){
            if(roleId==='2' && profileRole===3) alert("Currently coaches cant view parents children")
            else alert("Error getting children: " + error)

        }   
    }

    const getParents = async () => {
        if(roleId==='1') CURRENT_API = URL.COACH_URL;
        try{
            await Axios.get(`${CURRENT_API}/child/parents/${username}`, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            })
                .then(response => {
                    setRelations(response.data)
                    if(roleId==='1') CURRENT_API = URL.ADMIN_URL;
                })
        } catch(error){
            alert("Error getting parents: " + error)
        }
    }

    const updateHidden = (optional) => { //Update fields to hide
        working(true);
        const endpoint = `${PLAYER_URL}optional/shared/lhskage`;

        try{
            Axios.put(endpoint, optional, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            }).then(res => {
                working(false)
                if(res.status===202) alert("Updated visibility")
                else alert("Something went wrong. Try again later")
            });
        } catch(error){
            alert("Error updating visibility: " + error)
        } 
    }

    const updateBase = (base) => {
        working(true);
        const endpoint = `${URL.COACH_URL}/person/update/${baseInfo.id}`;
        try{
            Axios.put(endpoint, base, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            }).then(res =>{
                working(false)
                if(res.status===201) alert("Updated base information");
                else alert("Something went wrong. Try again later")
                
            });
        } catch(error){
            alert("Error updating base information: " + error)
        }
        
    }

    const updateOptional = (optional) => { //Fields parent changes
        let endpoint = `${URL.PARENT_URL}optional/update/${username}`
        if(roleId === "2"){
            endpoint = `${URL.COACH_URL}optional/update/${optionalInfo.id}`
        }

        working(true)
        try{
            Axios.put(endpoint, optional, {
                headers: {'Authorization': sessionStorage.getItem("token"), 'Content-type': 'application/json; charset=UTF-8'}
            }).then(res =>{
                working(false)
                if(res.status === 201) alert("Updated optional")
                else alert("Something went wrong. Try again later")
            });
        } catch(error){
            alert("Error updating optional information: " + error)
        }
    }


    const working = (working) => {
        setLoading2(working)
    }

    const childClick = (event) => {
        const childUser = JSON.parse(event.target.getAttribute("data-value"))
        history.push(childUser)
        window.location.reload();   
    }

    if(!access) return <div>You cant visit this profile</div>

    if(loading){
        return <div>Loading...</div>
    }    

    return (
        <div id ="profileBG">
        <div className="container">
            <LoadingOverlay active={loading2} spinner text="loading"> 
            <Card id ="margin-profile">
                <div className="row">
                    <div className="col-md-2">
                        <img src={picture} alt = "img" className="rounded-circle"/>
                    </div>
                    <div className="col">
                        <h2>{baseInfo.firstname+" "+baseInfo.lastname} </h2>
                    </div>
                </div>

                <h2>Base information</h2>
                <div className="row"><BaseInfo baseInfo={baseInfo} parent={parent} player={player}
                                        updateBase={updateBase}/></div>

                <h3>Optional information</h3> 
                <div className="row"><OptionalInfo optionalInfo={optionalInfo} parent={parent} player={player}
                                        updateHidden={updateHidden} updateOptional={updateOptional}/></div>
                
                {relations.length>0 && profileRole===4 && <div className="">
                    
                    <h3>Parents</h3>
                    <div className="row">
                        <div className="col" onClick={childClick} data-value={JSON.stringify(relations[0].parent_1)}>{relations[0].parent_1}</div>
                        <div className="col" onClick={childClick} data-value={JSON.stringify(relations[0].parent_2)}>{relations[0].parent_2}</div>
                    </div>
                </div>}
                {relations.length > 0 && profileRole===3 && <div className="">
                    <h3>Children</h3>
                    <div className="row">
                        {relations.map(relation => <div onClick={childClick} data-value={JSON.stringify(relation.child)} className="col">{relation.child}</div>)}
                    </div>
                </div>}
                {relations.length===0 && !profileRole===2 &&roleId==="2" && <div className="col">No relations</div>}
                {relations.length===0 && profileRole===3 && <div className="">No kids</div>}
                </Card>

            </LoadingOverlay>
        </div>
        </div>
    )
}

export default ProfilePage