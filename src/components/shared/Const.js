export const ADMIN_URL = 'https://schoolteams-gateway-zuul.herokuapp.com/administrator/api/v1/'
export const COACH_URL = 'https://schoolteams-gateway-zuul.herokuapp.com/coach/api/v1/';
export const PARENT_URL = 'https://schoolteams-gateway-zuul.herokuapp.com/parent/api/v1/';
export const PLAYER_URL = 'https://schoolteams-gateway-zuul.herokuapp.com/player/api/v1/';

export const LOGIN_URL = 'https://schoolteams-service-auth.herokuapp.com/api/v1/';
export const EMAIL_URL = 'https://schoolteams-service-mail.herokuapp.com/api/v1/';
export const REGISTER_URL = 'https://schoolteams-service-auth.herokuapp.com/api/v1/';


export let USER = '';



export default { ADMIN_URL, COACH_URL, PARENT_URL, PLAYER_URL, LOGIN_URL, EMAIL_URL, REGISTER_URL};
