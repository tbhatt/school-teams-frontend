import React from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

function LogoutButton(props){
    const history = useHistory();

    function logOut(){
        sessionStorage.clear();
        history.push('');
    }

    return (
        <Button variant="dark" onClick={logOut}>Log out</Button>
    )
}

export default LogoutButton;